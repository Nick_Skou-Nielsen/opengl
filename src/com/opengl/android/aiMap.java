package com.opengl.android;

import java.util.ArrayList;

import com.opengl.android.Geometric.Combine2D;
import com.opengl.android.Geometric.Combine3D;
import com.opengl.android.Geometric.TractorAddtional;
import com.opengl.android.Geometric.Trailer2D;
import com.opengl.android.Geometric.basicDrawer;
import com.opengl.android.Util.Constants;

import android.annotation.SuppressLint;
import android.content.Context;
import android.opengl.GLSurfaceView;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import fieldObjects.coordinate;
import geometricObjects.point;
import geometricObjects.spatialVarient;

/**Main class for the aiMap graphic engine, it needs to be set for the app to use it for drawing graphics.
 * It also includes the methods for setting the different objects and textures to be drawn and manipulated.
 * 
 * @author Nick Skou-Nielsen
 *
 */
@SuppressLint("NewApi")
public class aiMap extends GLSurfaceView {
	
	private final cameraHandler camera;
	private String UTM;
	private String UTMZone;
	private double zone;
	private GLRenderer Render;
	private point originOffset;
	AsyncTask<Void, Void, Void> update;
	private ScaleGestureDetector mScaleDetector;
	private float mScaleFactor = 1.f;
	private backGroundMap backGroundMap;

	/**Constructor for the aiMap, it will run if you just pass it the main context of the app.
	 * 
	 * @param context
	 */
	public aiMap(Context context) {
		super(context);
		camera = new cameraHandler();
		setEGLContextClientVersion(2);
		Render = new GLRenderer(context, new float[] {0.9f,0.9f,0.9f,0f});
		Render.setCamera(camera);
		this.setRenderer(Render);
		this.setRenderMode(RENDERMODE_CONTINUOUSLY);
		mScaleDetector = new ScaleGestureDetector(context, new ScaleListener());
	}
	/**Constructor for the aiMap, it will run if you just pass it the main context of the app and set of
	 * attributes from the layout.
	 * 
	 * @param context
	 */
	public aiMap(Context context, AttributeSet attrs) {
		super(context, attrs);
		camera = new cameraHandler();
		setEGLContextClientVersion(2);
		Render = new GLRenderer(context, new float[] {0.9f,0.9f,0.9f,0f});
		Render.setCamera(camera);
		this.setRenderer(Render);
		this.setRenderMode(RENDERMODE_CONTINUOUSLY);
		mScaleDetector = new ScaleGestureDetector(context, new ScaleListener());
	}
	/* (non-Javadoc)
	 * @see android.view.View#onTouchEvent(android.view.MotionEvent)
	 */
	
	public void switchMapMode(int newMode){
		Constants.currentMapMode = newMode;
	}
	
	public void setFieldCenter(float[] center){
		this.camera.fieldCenter = new float[]{center[0], center[1]};
	}
	
	float x1 = 0, x2, y1 = 0, y2, dx, dy;
	boolean scaleEventStarted = false;
	@Override
	public boolean onTouchEvent(MotionEvent ev) {
	    // Let the ScaleGestureDetector inspect all events.
	    mScaleDetector.onTouchEvent(ev);
	    switch(ev.getAction()){
	    
	    case(MotionEvent.ACTION_DOWN):
	    	x1 = ev.getX();
	    	y1 = ev.getY();
	    	scaleEventStarted = false;
	    break;

	    case(MotionEvent.ACTION_MOVE):
	    	if(!mScaleDetector.isInProgress() && !scaleEventStarted){
	    		x2 = ev.getX();
	        	y2 = ev.getY();
	        	float fac = (float) Math.pow(2, camera.getZoom()/2);
	        	dx = (x2-x1)*fac/500;
	        	dy = (y2-y1)*fac/500;
	        	x1 = x2;
	        	y1 = y2;
	        	this.camera.setPositionMovement(new float[]{dx,-dy});
		    	Log.i("Motion","Started movement: " + dx + ", " + dy);
		    	break;
	    	}else{
	    		x1 = x2;
	        	y1 = y2;
	        	scaleEventStarted = true;
	        	break;
	    	}
	    }
	    return true;
	}
	/**
	 * @author Nick Skou-Nielsen
	 *
	 */
	private class ScaleListener
    	extends ScaleGestureDetector.SimpleOnScaleGestureListener {
			@Override
			public boolean onScale(ScaleGestureDetector detector) {
				mScaleFactor *= Math.pow(detector.getScaleFactor(), -0.33);
				camera.setZoom(mScaleFactor);
				mScaleFactor = camera.getZoom();
				// Don't let the object get too small or too large.
				// Log.i("Touch", mScaleFactor + "");
//				invalidate();
				return true;
			}
	}

	/**
	 * @return The cameraHandler which can be used to set different parts of the view on the map.
	 */
	public cameraHandler getCamera() {
		return camera;
	}
	
	/**
	 * @param options
	 * @return
	 */
	public polygonDrawer addPolygon(elementOptions options) {
		polygonDrawer temp = new polygonDrawer();
		temp.setOptions(options);
		temp.drawOnMap();
		return temp;
	}
	
	public multiPolygonDrawer addPolygonMulti(elementOptions options) {
		multiPolygonDrawer temp = new multiPolygonDrawer();
		temp.setOptions(options);
		temp.drawOnMap();
		return temp;
	}
	
	/**
	 * @param options
	 * @return
	 */
	public lineDrawer addLine(elementOptions options) {
		lineDrawer temp = new lineDrawer();
		temp.setOptions(options);
		temp.drawOnMap();
		return temp;
	}
	
	/**
	 * @param options
	 * @return
	 */
	public pointDrawer addPoint(elementOptions options) {
		pointDrawer temp = new pointDrawer();
		temp.setOptions(options);
		temp.drawOnMap();
		return temp;
	}
	
	/**
	 * @param options
	 * @return
	 */
	public texture2DDrawer add2DTexture(elementOptions options) {
		texture2DDrawer temp = new texture2DDrawer();
		temp.setOptions(options);
		temp.drawOnMap();
		return temp;
	}
	
	/**
	 * @param options
	 * @return
	 */
	
	public texture3DDrawerCombine add3DCombine(elementOptions options){
		texture3DDrawerCombine temp = new texture3DDrawerCombine();
		temp.setOptions(options);
		temp.drawOnMap();
		return temp;
	}
	
	public texture2DDrawerCombine add2DCombine(elementOptions options){
		texture2DDrawerCombine temp = new texture2DDrawerCombine();
		temp.setOptions(options);
		temp.drawOnMap();
		return temp;
	}
	
	public texture3DDrawer add3DTexture(elementOptions options) {
		texture3DDrawer temp = new texture3DDrawer();
		temp.setOptions(options);
		temp.drawOnMap();
		return temp;
	}
	
	public texture2DDrawerTrailer add2DTrailer(elementOptions options){
		texture2DDrawerTrailer temp = new texture2DDrawerTrailer();
		temp.setOptions(options);
		temp.drawOnMap();
		return temp;
	}
	
	public texture3DDrawerTrailer add3DTrailer(elementOptions options){
		texture3DDrawerTrailer temp = new texture3DDrawerTrailer();
		temp.setOptions(options);
		temp.drawOnMap();
		return temp;
	}
	
	/**
	 * @param options
	 * @return
	 */
	public mapDrawer addMap(elementOptions options) {
		mapDrawer temp = new mapDrawer();
		temp.setOptions(options);
		temp.drawOnMap();
		return temp;
	}
	
	/** Handles the conversion from coordinates to UTM and their origin offset.
	 * @param newCoordinate Coordinate to be transformed.
	 * @return
	 */
	private point coordinateToUTM(coordinate newCoordinate) {
		//TODO Find a way to convert coordinate to specific UTMZone 
		if(UTM==null) {
			setOffset(newCoordinate);
		}
		point np = newCoordinate.coordinateToPoint();
		return new point(np.x-originOffset.x,np.y-originOffset.y);
	}
	
	private synchronized void setOffset(final coordinate newCoordinate){
		if(UTM==null) {
			UTMZone = newCoordinate.getUTMZone();
			zone = newCoordinate.getZone();
			originOffset = newCoordinate.coordinateToPoint();
			UTM = newCoordinate.getUTM();
//			elementOptions options = new elementOptions();
//			options.getPoints().add(newCoordinate);
//			mapDrawer map = addMap(options);
//			backGroundMap = new backGroundMap(100);
		}
	}
	
	/**Basic class to handle drawing objects and textures on the map. Any specific object or texture will
	 * an extension of this class.
	 * @author Nick Skou-Nielsen
	 *
	 */
	public class mapElement{
		
		protected elementOptions options = new elementOptions();
		protected basicDrawer drawer;
		public boolean removed = false;
				
		public mapElement() {

		}
		
		/**
		 * 
		 */
		protected void drawOnMap() {
			drawer = Render.add(this);
		}
		public boolean hasBeenDrawn(){
			return drawer!=null;
		}
		public boolean hasBeenRemoved(){
			return removed;
		}
		/**
		 * 
		 */
		public void replace() {
			Render.replace(this);
		}
		/**
		 * 
		 */
		public void removeFromMap() {
			Render.remove(this);
			drawer = null;
		}
		
		/**
		 * @return
		 */
		public elementOptions getOptions() {
			return options;
		}
		/**
		 * @param options
		 */
		public void setOptions(elementOptions options) {
			this.options = options;
		}
		/**
		 * 
		 */
		public void moveToFront() {
			Render.moveToFront(this);
		}
		
	}
	
	/**
	 * @author Administrator
	 *
	 */
	public class polygonDrawer extends mapElement{
		
	}
	
	/**
	 * @author Administrator
	 *
	 */
	public class multiPolygonDrawer extends mapElement{
		
	}
	
	/**
	 * @author Administrator
	 *
	 */
	public class lineDrawer extends mapElement{
		
	}
	
	/**Used to draw 2d textures on the map. The textures can be moved and rotated live.
	 * @author Nick Skou-Nielsen
	 *
	 */
	public class texture2DDrawer extends mapElement{
		/**
		 * @param location Sets the location of the texture on the map.
		 * @param rotation Sets the rotation of the texture on the map.
		 */
		public void setLocationAndRotation(coordinate location, float rotation) {
			if(drawer!=null) {
				point temp = coordinateToUTM(location);
				((TractorAddtional) drawer).setLocation(new float[] {(float) temp.x,(float) temp.y});
				((TractorAddtional) drawer).setRotation(rotation);
			}
			this.getOptions().setPosition(location);
			this.getOptions().setRotation(rotation);
		}
		
	}
	
	public class texture3DDrawerCombine extends mapElement{
		public void useSpout(boolean spoutOn) {
			if(drawer!=null) {
				((Combine3D) drawer).useSpout(spoutOn);
			}
			this.getOptions().setUseSpout(spoutOn);
		}
	}
	
	public class texture3DDrawerTrailer extends mapElement{
		
	}
	
	public class texture2DDrawerCombine extends mapElement{
		public void useSpout(boolean spoutOn) {
			if(drawer!=null) {
				((Combine2D) drawer).useSpout(spoutOn);
			}
			this.getOptions().setUseSpout(spoutOn);
		}
		public void setLocationAndRotation(coordinate location, float rotation) {
			if(drawer!=null) {
				point temp = coordinateToUTM(location);
				((Combine2D) drawer).setLocation(new float[] {(float) temp.x,(float) temp.y});
				((Combine2D) drawer).setRotation(rotation);
			}
			this.getOptions().setPosition(location);
			this.getOptions().setRotation(rotation);
		}
	}
	
	public class texture2DDrawerTrailer extends mapElement{
		public void setLoad(float load) {
			if(drawer!=null) {
				((Trailer2D) drawer).setLoad(load);
			}
			this.getOptions().setLoad(load);
		}
		public void setLocationAndRotation(coordinate location, float rotation) {
			if(drawer!=null) {
				point temp = coordinateToUTM(location);
				((Trailer2D) drawer).setLocation(new float[] {(float) temp.x,(float) temp.y});
				((Trailer2D) drawer).setRotation(rotation);
			}
			this.getOptions().setPosition(location);
			this.getOptions().setRotation(rotation);
		}
	}
	
	/**Adds an object which will change between 2d and 3d, depending on the tilt of the map from the 
	 * cameraHandler. The object will always be in the same relative position of the map.
	 * 
	 * @author Nick Skou-Nielsen
	 *
	 */
	public class texture3DDrawer extends mapElement{
		
	}
	
	/** Draws a point on the map.
	 * @author Nick Skou-Nielsen
	 *
	 */
	public class pointDrawer extends mapElement{
		
	}
	
	/**
	 * @author Administrator
	 *
	 */
	public class mapDrawer extends mapElement{
		
	}
	
	/**
	 * @author Administrator
	 *
	 */
	public class elementOptions{
		//Set the options that will be used to create the mapElements that will be shown on the map.
		private float size = 1f;
		private float[] position = new float[] {0f, 0f};
		private float stroakThicknes = 0.1f;
		private float[] color = new float[] {1f, 1f, 1f, 0f};
		private float[] stroakColor = new float[] {0f, 0f, 0f, 0f};
		private ArrayList<coordinate> points = new ArrayList<coordinate>();
		private ArrayList<coordinate> edge = new ArrayList<coordinate>();
		private ArrayList<ArrayList<coordinate>> multiEdge = new ArrayList<ArrayList<coordinate>>();
		private ArrayList<ArrayList<coordinate>> multiPoints = new ArrayList<ArrayList<coordinate>>();
		private boolean useEdge = false;
		private float load;
		
		/**
		 * @return the load
		 */
		public float getLoad() {
			return load;
		}
		/**
		 * @param load the load to set
		 */
		public void setLoad(float load) {
			this.load = load;
		}
		/**
		 * @return the usePolyEdge
		 */
		public boolean getUseEdge() {
			return useEdge;
		}
		/**
		 * @param usePolyEdge the usePolyEdge to set
		 */
		public void setUsePolyEdge(boolean useEdge) {
			this.useEdge = useEdge;
		}
		/**
		 * @return the edge
		 */
		public ArrayList<coordinate> getEdgeB() {
			final ArrayList<coordinate> outEdge = new ArrayList<coordinate>();
			for (int i = 0; i < edge.size(); i++) {
				outEdge.add(new coordinate(edge.get(i).latitude, edge.get(i).longitude));
			}
			return outEdge;
		}
		/**
		 * @param edge the edge to set
		 */
		public void setEdge(ArrayList<coordinate> edge) {
			
			this.edge = edge;
		}
		/**
		 * @return the multiEdge
		 */
		public ArrayList<ArrayList<coordinate>> getMultiEdgeB() {
			final ArrayList<ArrayList<coordinate>> me = new ArrayList<ArrayList<coordinate>>();
			for (int i = 0; i < multiEdge.size(); i++) {
				final ArrayList<coordinate> oe = new ArrayList<coordinate>();
				final ArrayList<coordinate> ze = multiEdge.get(i);
				for (int j = 0; j < ze.size(); j++) {
					oe.add(new coordinate(ze.get(j).latitude, ze.get(j).longitude));
				}
				me.add(oe);
			}

			return me;
		}
		/**
		 * @param multiEdge the multiEdge to set
		 */
		public void setMultiEdge(ArrayList<ArrayList<coordinate>> multiEdge) {
			this.multiEdge = multiEdge;
		}
		/**
		 * @return the multiPoints
		 */
		public ArrayList<ArrayList<coordinate>> getMultiPointsB() {
			final ArrayList<ArrayList<coordinate>> me = new ArrayList<ArrayList<coordinate>>();
			for (int i = 0; i < multiPoints.size(); i++) {
				final ArrayList<coordinate> oe = new ArrayList<coordinate>();
				final ArrayList<coordinate> ze = multiPoints.get(i);
				for (int j = 0; j < ze.size(); j++) {
					oe.add(new coordinate(ze.get(j).latitude, ze.get(j).longitude));
				}
				me.add(oe);
			}

			return me;
		}
		/**
		 * @param multiPoints the multiPoints to set
		 */
		public void setMultiPoints(ArrayList<ArrayList<coordinate>> multiPoints) {
			this.multiPoints = multiPoints;
		}
		private ArrayList<ArrayList<coordinate>> holes = new ArrayList<ArrayList<coordinate>>();
		private int textureResource2D = 0;
		private int textureResource3D = 0;
		/**
		 * @return the textureResource2D_2
		 */
		public int getTextureResource2D_2() {
			return textureResource2D_2;
		}
		/**
		 * @param textureResource2D_2 the textureResource2D_2 to set
		 */
		public void setTextureResource2D_2(int textureResource2D_2) {
			this.textureResource2D_2 = textureResource2D_2;
		}
		/**
		 * @return the textureResource3D_2
		 */
		public int getTextureResource3D_2() {
			return textureResource3D_2;
		}
		/**
		 * @param textureResource3D_2 the textureResource3D_2 to set
		 */
		public void setTextureResource3D_2(int textureResource3D_2) {
			this.textureResource3D_2 = textureResource3D_2;
		}
		/**
		 * @return the textureResource2D_3
		 */
		public int getTextureResource2D_3() {
			return textureResource2D_3;
		}
		/**
		 * @param textureResource2D_3 the textureResource2D_3 to set
		 */
		public void setTextureResource2D_3(int textureResource2D_3) {
			this.textureResource2D_3 = textureResource2D_3;
		}
		/**
		 * @return the textureResource3D_3
		 */
		public int getTextureResource3D_3() {
			return textureResource3D_3;
		}
		/**
		 * @param textureResource3D_3 the textureResource3D_3 to set
		 */
		public void setTextureResource3D_3(int textureResource3D_3) {
			this.textureResource3D_3 = textureResource3D_3;
		}
		private int textureResource2D_2 = 0;
		private int textureResource3D_2 = 0;
		private int textureResource2D_3 = 0;
		private int textureResource3D_3 = 0;
		private boolean useSpout = false;
		/**
		 * @return the useSpout
		 */
		public boolean isUseSpout() {
			return useSpout;
		}
		/**
		 * @param useSpout the useSpout to set
		 */
		public void setUseSpout(boolean useSpout) {
			this.useSpout = useSpout;
		}
		private float rotation = 0;
		private int layer = 0;
		private float anchorX2d = 0.5f;
		private float anchorY2d = 0.5f;
		private float anchorX3d = 0.5f;
		private float anchorY3d = 0.5f;
		/**
		 * @return Returns the anchor in the x dimension for the 2d texture in percentage.
		 */
		public float getAnchorX2d() {
			return anchorX2d;
		}
		/**
		 * Sets the anchor location in the x dimension for the 2d texture in percentage. So 0.5f would the center.
		 * @param anchorX2d
		 */
		public void setAnchorX2d(float anchorX2d) {
			this.anchorX2d = anchorX2d;
		}
		/**
		 * @return Returns the anchor in the y dimension for the 2d texture in percentage.
		 */
		public float getAnchorY2d() {
			return anchorY2d;
		}
		/**
		 * @param anchorY2d
		 */
		public void setAnchorY2d(float anchorY2d) {
			this.anchorY2d = anchorY2d;
		}
		/**
		 * @return
		 */
		public float getAnchorX3d() {
			return anchorX3d;
		}
		/**
		 * @param anchorX3d
		 */
		public void setAnchorX3d(float anchorX3d) {
			this.anchorX3d = anchorX3d;
		}
		/**
		 * @return
		 */
		public float getAnchorY3d() {
			return anchorY3d;
		}
		/**
		 * @param anchorY3d
		 */
		public void setAnchorY3d(float anchorY3d) {
			this.anchorY3d = anchorY3d;
		}
		/**
		 * @param layer
		 */
		public void setLayer(int layer) {
			this.layer = layer;
		}
		/**
		 * @return
		 */
		public int getLayer() {
			return layer;
		}
		/**
		 * @return
		 */
		public float getSize() {
			return size;
		}
		/**
		 * @param size
		 */
		public void setSize(float size) {
			this.size = size;
		}
		/**
		 * @return
		 */
		public float[] getPosition() {
			return position;
		}
		/**
		 * @param position
		 */
		public void setPosition(coordinate position) {
			point temp = coordinateToUTM(position);
			this.position = new float[] {(float) temp.x,(float) temp.y};
		}
		/**
		 * @return
		 */
		public float getStroakThicknes() {
			return stroakThicknes;
		}
		/**
		 * @param stroakThicknes
		 */
		public void setStroakThicknes(float stroakThicknes) {
			this.stroakThicknes = stroakThicknes;
		}
		/**
		 * @return
		 */
		public float[] getColor() {
			return color;
		}
		/**
		 * @param color
		 */
		public void setColor(float[] color) {
			this.color = color;
		}
		/**
		 * @param hexInt
		 */
		public void setColor(int hexInt) {
			String rgba = Integer.toHexString(hexInt);
		    for (int i = 1; i < 4; i++) {
		        color[i-1] = (float) Integer.parseInt(rgba.substring(i * 2, i * 2 + 2), 16)/255;
		    }
		    	color[3] = (float) Integer.parseInt(rgba.substring(0 * 2, 0 * 2 + 2), 16)/255;
		}
		/**
		 * @return
		 */
		public float[] getStroakColor() {
			return stroakColor;
		}
		/**
		 * @param stroakColor
		 */
		public void setStroakColor(float[] stroakColor) {
			this.stroakColor = stroakColor;
		}
		/**
		 * @param hexInt
		 */
		public void setStroakColor(int hexInt) {
			String rgba = Integer.toHexString(hexInt);
			for (int i = 1; i < 4; i++) {
				stroakColor[i-1] = (float) Integer.parseInt(rgba.substring(i * 2, i * 2 + 2), 16)/255;
		    }
			stroakColor[3] = (float) Integer.parseInt(rgba.substring(0 * 2, 0 * 2 + 2), 16)/255;
		}
		/**
		 * @return
		 */
		public ArrayList<coordinate> getPointsB() {
			final ArrayList<coordinate> outEdge = new ArrayList<coordinate>();
			for (int i = 0; i < points.size(); i++) {
				outEdge.add(new coordinate(points.get(i).latitude, points.get(i).longitude));
			}
			return outEdge;
		}
		/**
		 * @param points
		 */
		public void setPoints(ArrayList<coordinate> points) {
			this.points = points;
		}
		/**
		 * @return
		 */
		public ArrayList<ArrayList<coordinate>> getHolesB() {
			final ArrayList<ArrayList<coordinate>> me = new ArrayList<ArrayList<coordinate>>();
			for (int i = 0; i < holes.size(); i++) {
				final ArrayList<coordinate> oe = new ArrayList<coordinate>();
				final ArrayList<coordinate> ze = holes.get(i);
				for (int j = 0; j < ze.size(); j++) {
					oe.add(new coordinate(ze.get(j).latitude, ze.get(j).longitude));
				}
				me.add(oe);
			}

			return me;
		}
		/**
		 * @param holes
		 */
		public void setHoles(ArrayList<ArrayList<coordinate>> holes) {
			this.holes = holes;
		}
		/**
		 * @return
		 */
		public int getTextureResource2D() {
			return textureResource2D;
		}
		/**
		 * @param textureResource2D
		 */
		public void setTextureResource2D(int textureResource2D) {
			this.textureResource2D = textureResource2D;
		}
		/**
		 * @return
		 */
		public int getTextureResource3D() {
			return textureResource3D;
		}
		/**
		 * @param textureResource3D
		 */
		public void setTextureResource3D(int textureResource3D) {
			this.textureResource3D = textureResource3D;
		}
		/**
		 * @return
		 */
		public float getRotation() {
			return rotation;
		}
		/**
		 * @param rotation
		 */
		public void setRotation(float rotation) {
			this.rotation = rotation;
		}
		/**
		 * @return
		 */
		public point originOffset() {
			return originOffset.copy();
		}
		/**
		 * @return
		 */
		public ArrayList<point> getPointList(){
			ArrayList<point> out = new ArrayList<point>();
			for(int i=0; i<points.size();i++) {
				out.add(coordinateToUTM(points.get(i)));
			}
			return out;
		}
		public ArrayList<ArrayList<point>> getPointListMulti(){
			ArrayList<ArrayList<point>> out = new ArrayList<ArrayList<point>>();
			for(int j=0;j<multiPoints.size();j++){
				out.add(new ArrayList<point>());
				for(int i=0; i<multiPoints.get(j).size();i++) {
					out.get(j).add(coordinateToUTM(multiPoints.get(j).get(i)));
				}
			}
			return out;
		}
		public ArrayList<ArrayList<point>> getPointListEdgeMulti(){
			ArrayList<ArrayList<point>> out = new ArrayList<ArrayList<point>>();
			for(int j=0;j<multiEdge.size();j++){
				out.add(new ArrayList<point>());
				for(int i=0; i<multiEdge.get(j).size();i++) {
					out.get(j).add(coordinateToUTM(multiEdge.get(j).get(i)));
				}
			}
			return out;
		}
		public ArrayList<point> getPointListEdge(){
			ArrayList<point> out = new ArrayList<point>();
			for(int i=0; i<edge.size();i++) {
				out.add(coordinateToUTM(edge.get(i)));
			}
			return out;
		}
	}
	
	public class cameraHandler {
		private float zoom = 4;
		public float bearing = 0;
		public float tilt = (float) 67.5;
		public float[] position = new float[] {0f, 0f};
		public float[] positionMovement = new float[] {0f, 0f};
		public float[] fieldCenter = new float[] {0f, 0f};
		public void setPosition(coordinate position) {
			point temp = coordinateToUTM(position);
			this.position = new float[] {(float) temp.x,(float) temp.y};
//			backGroundMap.update(temp);
		}
		public void setPositionMovement(float[] movement) {
			this.positionMovement = new float[] {positionMovement[0] + movement[0],positionMovement[1] + movement[1]};
//			backGroundMap.update(temp);
		}
		public void setZoom(float i) {
			zoom = Math.max(Math.min(i, 26),8);
		}
		public float getZoom() {
			return zoom;
		}
	}
	
	/**
	 * @author Administrator
	 *
	 */
	public class backGroundMap {
		private final ArrayList<ArrayList<Boolean>> addedMaps = new ArrayList<ArrayList<Boolean>>();
		private final ArrayList<ArrayList<mapDrawer>> mapPieces = new ArrayList<ArrayList<mapDrawer>>();
		private float mapDimensions;
		private int[] center = new int[] {0,0};
		private int[] dimensions = new int[] {0,0,0,0};
		
		public backGroundMap(float dimension){
			mapDimensions = dimension;
			addedMaps.add(newArrayAdded(0,new int[]{0}));
			mapPieces.add(newArrayMap(0,new int[]{0}));
		}
		
		private mapDrawer newMap(int i, int j){
			mapDrawer temp = new mapDrawer();
			elementOptions options = new elementOptions();
			point tempPoint = new point(mapDimensions*i+originOffset.x,mapDimensions*j+originOffset.y);
			coordinate tempCoord = tempPoint.toCoordinate(zone, UTM);
			ArrayList<coordinate> tcs = options.getPointsB();
			tcs.add(tempCoord);
			options.setPoints(tcs);
			options.setSize(mapDimensions);
			temp.setOptions(options);
			return temp;
		}
		protected spatialVarient newSpatialVarient(){
			return new spatialVarient();
		}
		
		private ArrayList<Boolean> newArrayAdded(int i, int[] j){
			ArrayList<Boolean> newArray = new ArrayList<Boolean>();
			for(int k=0;k<j.length;k++){
				newArray.add(false);
			}
			return newArray;
		}		
		private ArrayList<mapDrawer> newArrayMap(int i, int[] j){
			ArrayList<mapDrawer> newArray = new ArrayList<mapDrawer>();
			for(int k=0;k<j.length;k++){
				newArray.add(newMap(i,j[k]));
			}
			return newArray;
		}
		public void update(point newPoint) {
			int[] gridPoint = new int[]{(int) Math.floor(newPoint.x/mapDimensions)-center[0],(int) Math.floor(newPoint.y/mapDimensions)-center[1]};
			
			
			for(int k=-2;k<3;k++) {
				for(int l=-2;l<3;l++) {
					int[] gridCell = new int[] {gridPoint[0]+k,gridPoint[0]+l};
					if (gridCell[0]<this.dimensions[0]){
						int[] y = new int[this.dimensions[3]-this.dimensions[1]+1];
						for(int j=this.dimensions[1];j<=this.dimensions[3];j++){
							y[j-this.dimensions[1]] = j;
						}
						for(int i=this.dimensions[0]-1;i>=gridCell[0];i--){
							this.addedMaps.add(0,newArrayAdded(i,y));
							this.mapPieces.add(0,newArrayMap(i,y));
						}
						this.dimensions[0] = gridCell[0];
					}
					if (gridCell[1]<this.dimensions[1]){
						for(int i=this.dimensions[1]-1;i>=gridCell[1];i--){
							for(int j=this.dimensions[0];j<=this.dimensions[2];j++){
								this.addedMaps.get(j-this.dimensions[0]).add(0,false);
								this.mapPieces.get(j-this.dimensions[0]).add(0,newMap(j,i));
							}
						}
						this.dimensions[1] = gridCell[1];
					}
					if(gridCell[0]>this.dimensions[2]){
						int[] y = new int[this.dimensions[3]-this.dimensions[1]+1];
						for(int j=this.dimensions[1];j<=this.dimensions[3];j++){
							y[j-this.dimensions[1]] = j;
						}
						for(int i=this.dimensions[2]+1;i<=gridCell[0];i++){
							this.addedMaps.add(0,newArrayAdded(i,y));
							this.mapPieces.add(0,newArrayMap(i,y));
						}
						this.dimensions[2]=gridCell[0];
					}
					if(gridCell[1]>this.dimensions[3]){
						for(int i=this.dimensions[3]+1;i<=gridCell[1];i++){
							for(int j=this.dimensions[0];j<=this.dimensions[2];j++){
								this.addedMaps.get(j-this.dimensions[0]).add(false);
								this.mapPieces.get(j-this.dimensions[0]).add(newMap(j, i));
							}
						}
						this.dimensions[3]=gridCell[1];
					}
					if(!this.addedMaps.get(gridCell[0]-this.dimensions[0]).get(gridCell[1]-this.dimensions[1])) {
						this.mapPieces.get(gridCell[0]-this.dimensions[0]).get(gridCell[1]-this.dimensions[1]).drawOnMap();
						this.addedMaps.get(gridCell[0]-this.dimensions[0]).set(gridCell[1]-this.dimensions[1], true);
					}
				}
			}
			
		}
		
	}
		
	
	public void updateFrame() {
//		if(update!=null) {
//			if(update.getStatus().equals(AsyncTask.Status.FINISHED)) {
//				update = new AsyncTask<Void, Void, Void>() {
//
//					@Override
//					protected Void doInBackground(Void... params) {
//						Render.handleUpdates();
//						return null;
//					}
//					
//				}.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,null, null, null);
//			}
//		}else {
//			update = new AsyncTask<Void, Void, Void>() {
//
//				@Override
//				protected Void doInBackground(Void... params) {
//					Render.handleUpdates();
//					return null;
//				}
//				
//			}.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,null, null, null);
//		}
//		this.requestRender();
	}
}
