package com.opengl.android;


import java.util.Timer;
import java.util.TimerTask;

import com.opengl.android.Geometric.ColorPoly;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ConfigurationInfo;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Toast;
import geometricObjects.arc;
import geometricObjects.direction;
import geometricObjects.point;
import geometricObjects.polygon;

public class MainActivity extends Activity {
	private GLSurfaceView glSurfaceView;
	private boolean rendererSet = false;
	private Timer timer;
	private TimerTask timerTask;
	protected final Handler handler = new Handler();
	private static GLRenderer Render;
	
	private point[] points = new point[]{new point(0,0),new point(0,1),new point(0,2),new point(0,3),new point(0,4),new point(0,5),new point(1,6),new point(2,7),new point(3,8),new point(4,9),new point(5,10),new point(6,10),new point(7,10),new point(8,10),new point(9,10),new point(10,10)};
	private direction[] directions = new direction[]{new direction(0,1),new direction(0,1),new direction(0,1),new direction(0,1),new direction(0,1),new direction(0,1),new direction(1,1),new direction(1,1),new direction(1,1),new direction(1,1),new direction(1,1),new direction(1,0),new direction(1,0),new direction(1,0),new direction(1,0),new direction(1,0),new direction(1,0),new direction(1,0)};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		glSurfaceView =(GLSurfaceView) findViewById(R.id.mapContent);
		
		final ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		final ConfigurationInfo configurationInfo = activityManager.getDeviceConfigurationInfo();
		final boolean supportsEs2 =
				configurationInfo.reqGlEsVersion >= 0x20000
				|| (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1
				&& (Build.FINGERPRINT.startsWith("generic")
				|| Build.FINGERPRINT.startsWith("unknown")
				|| Build.MODEL.contains("google_sdk")
				|| Build.MODEL.contains("Emulator")
				|| Build.MODEL.contains("Android SDK built for x86")));
		
		if (supportsEs2) {
			// Request an OpenGL ES 2.0 compatible context.
			glSurfaceView.setEGLContextClientVersion(2);
			// Assign our renderer.
			
			if(Render==null) {
				Render = new GLRenderer(this,new float[] {1f, 1f, 1f, 1f});
			}
			glSurfaceView.setRenderer(Render);
			rendererSet = true;
			} else {
				Toast.makeText(this, "This device does not support OpenGL ES 2.0.",
						Toast.LENGTH_LONG).show();
				return;
			}
		
		
		((RadioGroup) findViewById(R.id.orientationViewRG)).setOnCheckedChangeListener(
				new OnCheckedChangeListener(){
					
					@Override
					public void onCheckedChanged(RadioGroup group, int checkedId){
						
						if(checkedId == R.id.twoD){
							switchTo2D();
							
						}
						else{
							switchTo3D();
							

						}
						
					}

				});
		((RadioGroup) findViewById(R.id.mapViewRG)).setOnCheckedChangeListener(
				new OnCheckedChangeListener(){
					
					@Override
					public void onCheckedChanged(RadioGroup group, int checkedId){
						
							if (checkedId == R.id.mapViewMap) {
								roadMap();
							} else if (checkedId == R.id.mapViewSat) {
								satMap();
							} else if (checkedId == R.id.mapViewnone) {
								noMap();
							}

					}

				});		
	}
	
	public void switchTo2D(){
		Render.pause();
		Render.set3d(false);
		Render.unPause();
		Toast.makeText(getBaseContext(), "switch to 2D", Toast.LENGTH_LONG).show();
	}
	
	public void switchTo3D(){
		Render.pause();
		Render.set3d(true);
		Render.unPause();
		Toast.makeText(getBaseContext(), "switch to 3D", Toast.LENGTH_LONG).show();
	}
	
	public void satMap(){
		Toast.makeText(getBaseContext(), "satalite map", Toast.LENGTH_LONG).show();
	}
	
	public void roadMap(){
		Toast.makeText(getBaseContext(), "road map", Toast.LENGTH_LONG).show();
	}
	public void noMap(){
		Toast.makeText(getBaseContext(), "no map", Toast.LENGTH_LONG).show();
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void minusZoomMap(View view){
		Render.pause();
		Render.changeZoom(Render.getZoom()+1);
		Render.unPause();
		Toast.makeText(this, "Zoom out", Toast.LENGTH_LONG).show();
	}
	
	public void plusZoomMap(View view){
		Render.pause();
		Render.changeZoom(Math.max(Render.getZoom()-1, 0));
		Render.unPause();
		Toast.makeText(this, "Zoom in", Toast.LENGTH_LONG).show();
	}
	
	public void queryStartNav(View view){
		if(timer == null){
			Render.pause();
			Render.unPause();
			timer = new Timer();
			initialiseTimer();
			timer.schedule(timerTask, 0, (long) (1000/1));
		}
	}
	int pointCounter = 0;
	public void initialiseTimer(){
		pointCounter=0;
		timerTask = new TimerTask() {
			public void run() {

				handler.post(new Runnable() {
					
					public void run() {
						if(pointCounter<points.length){
							moveMap(points[pointCounter], directions[pointCounter]);
							pointCounter++;
						}
						else{
							routeFinished();
						}
					}
				});
			}
		};
	}
	
	public void routeFinished(){
		timer.cancel();
		timer = null;
		Render.pause();
		Render.unPause();
	}
	
	public void moveMap(point p1, direction d1){
		Render.move(new float[] {(float) p1.x, (float) p1.y}, (float) d1.getBearing());
		Toast.makeText(this, "Move to point:"+p1.toShortJSON()+", direction:"+d1.toShortJSON(), Toast.LENGTH_SHORT).show();
	}
	
	
	
}
