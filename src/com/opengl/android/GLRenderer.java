package com.opengl.android;

import static android.opengl.GLES20.GL_COLOR_BUFFER_BIT;
import static android.opengl.GLES20.GL_ONE_MINUS_SRC_ALPHA;
import static android.opengl.GLES20.GL_SRC_ALPHA;
import static android.opengl.GLES20.glBlendFunc;
import static android.opengl.GLES20.glClear;
import static android.opengl.GLES20.glClearColor;
import static android.opengl.GLES20.glEnable;
import static android.opengl.GLES20.glViewport;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import com.opengl.android.aiMap.cameraHandler;
import com.opengl.android.aiMap.lineDrawer;
import com.opengl.android.aiMap.mapDrawer;
import com.opengl.android.aiMap.mapElement;
import com.opengl.android.aiMap.multiPolygonDrawer;
import com.opengl.android.aiMap.pointDrawer;
import com.opengl.android.aiMap.polygonDrawer;
import com.opengl.android.aiMap.texture2DDrawer;
import com.opengl.android.aiMap.texture2DDrawerCombine;
import com.opengl.android.aiMap.texture2DDrawerTrailer;
import com.opengl.android.aiMap.texture3DDrawer;
import com.opengl.android.aiMap.texture3DDrawerCombine;
import com.opengl.android.aiMap.texture3DDrawerTrailer;
import com.opengl.android.Engines.ColorShaderProgram;
import com.opengl.android.Engines.TextureShaderProgram;
import com.opengl.android.Geometric.ColorLine;
import com.opengl.android.Geometric.ColorMultiPoly;
import com.opengl.android.Geometric.ColorPoint;
import com.opengl.android.Geometric.ColorPoly;
import com.opengl.android.Geometric.Combine2D;
import com.opengl.android.Geometric.Combine3D;
import com.opengl.android.Geometric.Tractor;
import com.opengl.android.Geometric.TractorAddtional;
import com.opengl.android.Geometric.Trailer2D;
import com.opengl.android.Geometric.Trailer3D;
import com.opengl.android.Geometric.basicDrawer;
import com.opengl.android.Geometric.mapKortforsyning;
import com.opengl.android.Util.PerspectiveHandler;

import android.annotation.SuppressLint;
import android.content.Context;
import android.opengl.GLSurfaceView.Renderer;
import android.os.AsyncTask;
import android.util.Log;
import geometricObjects.arc;
import geometricObjects.point;
import geometricObjects.polygon;

@SuppressLint("NewApi")
public class GLRenderer implements Renderer {
	
	//This is where we set what is drawn and the order of it
	
	private final float[] projectionMatrixField = new float[16]; //Projection matrix for the field
	private final Context context;
	private final float[] backgroundColour; //Handles the background color RGB + alpha
	private final PerspectiveHandler perspective = new PerspectiveHandler();
	private long startTime = System.currentTimeMillis();
	private long framerate = 1000/10;
	private boolean pause = false;
	private cameraHandler camera;
	private boolean updateProgress = false;
	private ExecutorService AIExecutor = Executors.newFixedThreadPool(20);
	private ExecutorService CommiterPool = Executors.newFixedThreadPool(2);
		
	private ColorShaderProgram colorShaderProgram;
	private TextureShaderProgram textureProgram;
	private final ArrayList<ArrayList<basicDrawer>> figures = new ArrayList<ArrayList<basicDrawer>>();
	private final ArrayList<basicDrawer> figuresToBeAdded = new ArrayList<basicDrawer>();
	private final ArrayList<basicDrawer> figuresToBeUpdated = new ArrayList<basicDrawer>();
	private final ArrayList<basicDrawer> figuresToBeInFront = new ArrayList<basicDrawer>();
	private Map<mapElement,basicDrawer> mapReference = new HashMap<mapElement,basicDrawer>();
	private ArrayList<mapElement> elementsToBeRemoved = new ArrayList<mapElement>();
	private ArrayList<mapElement> elementsToBeMoved = new ArrayList<mapElement>();
	private ArrayList<mapElement> elementsToBeAdded = new ArrayList<mapElement>();
	private boolean added = false;
	private boolean removed = false;
	private AsyncTask<Void, Void, Void> update;
	
	public GLRenderer(Context context,float[] backgroundColour) {
		this.context = context;
		this.backgroundColour = backgroundColour;
	}
	
	public void setCamera(cameraHandler camera) {
		this.camera = camera;
	}
	
	@Override
	public void onDrawFrame(GL10 arg0) {
		try {
//		// Clear the rendering surface.
//		long endTime = System.currentTimeMillis();
//	    long dt = endTime - startTime;
//	    if (dt < framerate) {
//	    	try {
//				Thread.sleep((framerate));
//			} catch (InterruptedException e) {
//			}
//	    }
//	    startTime = System.currentTimeMillis();
	    updatePerspective();
//	    updateFrame();
	    
		if(update!=null && !updateProgress) {
		if(update.getStatus().equals(AsyncTask.Status.FINISHED)) {
			updateProgress=true;
			update = new AsyncTask<Void, Void, Void>() {

				@Override
				protected Void doInBackground(Void... params) {
					handleUpdates();
					updateProgress=false;
					return null;
				}
				
			}.executeOnExecutor(CommiterPool,null, null, null);
		}
	}else {
		updateProgress=true;
		update = new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... params) {
				handleUpdates();
				updateProgress=false;
				return null;
			}
			
		}.executeOnExecutor(CommiterPool,null, null, null);
		
	}
		ArrayList<basicDrawer> temp = new ArrayList<basicDrawer>();
		temp.addAll(figuresToBeUpdated);
		updateAll(temp);
		figuresToBeUpdated.removeAll(temp);
		glClear(GL_COLOR_BUFFER_BIT);
		
		drawAll();
		return;
		}catch(Exception e) {
			Log.e("ERROR", e.getLocalizedMessage(),e);
		}
	}
	
	
	public synchronized void handleUpdates() {
		ArrayList<mapElement> temp = new ArrayList<mapElement>();
		temp.addAll(elementsToBeAdded);
		elementsToBeAdded.removeAll(temp);
		for(int i=0;i<temp.size();i++) {
			add(temp.get(i));
		}
				if(removed) {
	    			removeFigures();
	    		}if(added){
					addFigures();
	    		}
	    		moveFigures();
	}

	@Override
	public void onSurfaceCreated(GL10 arg0, EGLConfig arg1) {
		glClearColor(backgroundColour[0], backgroundColour[1], backgroundColour[2], backgroundColour[3]);
		colorShaderProgram = new ColorShaderProgram(context);
		textureProgram = new TextureShaderProgram(context);
		for(int i=0;i<figures.size();i++) {
			try {
				updateAll(figures.get(i));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//Set such that the parts of the texture which is transparent is also drawn as transparent
		glEnable(GL10.GL_BLEND);
	    glBlendFunc(GL10.GL_ONE, GL10.GL_ONE_MINUS_SRC_ALPHA);
	    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}

    @Override
    public void onSurfaceChanged(GL10 glUnused, int width, int height) {
        // Set the OpenGL viewport to fill the entire surface.
        glViewport(0, -height/4*2, width, height+height/4*2);
        perspective.setDim(width, height+height/4*2);
        updateProjections();
    }
    
    public void changeZoom(float zoom) {
    	perspective.setZoom(zoom);
    	updateProjections();
    }
    public float getZoom() {
    	return perspective.getZoom();
    }
    public void set3d(boolean view3d) {
    	perspective.setView3d(view3d);
    	updateProjections();
    }
    public void updateProjections() {
    	perspective.getProjectionMatrixField(projectionMatrixField);
    	textureProgram.setMatrix(projectionMatrixField);
    	colorShaderProgram.setProjection(projectionMatrixField);
    }
    public void move(float[] location, float angle) {
    	perspective.setFieldTranslation(location);
    	perspective.setRotation(angle);
    	updateProjections();
    }


    public synchronized void updateAll(ArrayList<basicDrawer> list) throws MalformedURLException, IOException {
    	for(int i=0;i<list.size();i++) {
    		if(list.get(i)!=null){
    		if(list.get(i).getClass()==Tractor.class) {
    			((Tractor) list.get(i)).setProgram(textureProgram);
    			((Tractor) list.get(i)).update();
    		}
    		if(list.get(i).getClass()==ColorPoly.class || list.get(i).getClass()==ColorLine.class || list.get(i).getClass()==ColorPoint.class) {
    			((ColorPoly) list.get(i)).setProgram(colorShaderProgram);
    		}
    		if(list.get(i).getClass()==TractorAddtional.class) {
    			((TractorAddtional) list.get(i)).setProgram(textureProgram);
    			((TractorAddtional) list.get(i)).update();
    		}if(list.get(i).getClass()==mapKortforsyning.class) {
    			((mapKortforsyning) list.get(i)).setProgram(textureProgram);
    			((mapKortforsyning) list.get(i)).update();
    		}
    		if(list.get(i).getClass()==ColorMultiPoly.class){
    			((ColorMultiPoly) list.get(i)).setProgram(colorShaderProgram);
    		}
    		if(list.get(i).getClass()==Combine3D.class){
    			((Combine3D) list.get(i)).setProgram(textureProgram);
    			((Combine3D) list.get(i)).update();
    		}
    		if(list.get(i).getClass()==Combine2D.class){
    			((Combine2D) list.get(i)).setProgram(textureProgram);
    			((Combine2D) list.get(i)).update();
    		}
    		if(list.get(i).getClass()==Trailer2D.class){
    			((Trailer2D) list.get(i)).setProgram(textureProgram);
    			((Trailer2D) list.get(i)).setProgram2(colorShaderProgram);
    			((Trailer2D) list.get(i)).update();
    		}
    		if(list.get(i).getClass()==Trailer3D.class){
    			((Trailer3D) list.get(i)).setProgram(textureProgram);
    			((Trailer3D) list.get(i)).update();
    		}
    		list.get(i).visible = true;
    		}
		}
    }
    public void drawAll() {
    	ArrayList<basicDrawer> list = new ArrayList<basicDrawer>();
    	
    	for(int i=0;i<figures.size();i++) {
    		list.addAll(figures.get(i));
    	}
    	list.addAll(figuresToBeInFront);
    	for(int i=0;i<list.size();i++) {
    		if(list.get(i).visible) {
    			if(list.get(i).getClass()==Tractor.class) {
        			((Tractor) list.get(i)).setProgram(textureProgram);
        		}
        		list.get(i).draw();
    		}
		}
    }

    public void moveToFront(mapElement newElement) {
    	this.elementsToBeMoved.add(newElement);
    }
    
    public void setMaxFrameRate(long Frames) {
    	framerate = 1000/Frames;
    }
    public void pause() {
    	this.pause = true;
    }
    public void unPause() {
    	this.pause = false;
    }
    public synchronized basicDrawer add(final mapElement newElement) {
    	Log.i("AIMap", "Trying to Add graphic "+AIExecutor.toString());
    		AsyncTask<Void, Void, Void> as = new AsyncTask<Void, Void, Void>() {
    			basicDrawer temp = null;
    			@Override
    			protected Void doInBackground(Void... params) {
    				try{
    				if(newElement.getClass()==polygonDrawer.class) {
    					ArrayList<point> list = newElement.getOptions().getPointList();
    					point[] points = new point[list.size()];
    					list.toArray(points);
    					polygon newPolygon = new polygon(points);
    					polygon edge = null;
    					if(newElement.getOptions().getUseEdge()){
    						list = newElement.getOptions().getPointListEdge();
        					points = new point[list.size()];
        					list.toArray(points);
    						edge = new polygon(points);
    					}else{
    						edge = null;
    					}
    			    	temp = new ColorPoly(newPolygon,edge,newElement.getOptions().getColor(),newElement.getOptions().getStroakThicknes(),newElement.getOptions().getStroakColor(),newElement.getOptions().getLayer());
//    			    	mapReference.put(newElement, temp);
//    			    	figuresToBeAdded.add(temp);
    					
    				}else if(newElement.getClass()==lineDrawer.class) {
    					ArrayList<point> list = newElement.getOptions().getPointList();
    					if(list.size()==0) {
    						Log.i("OPENGL", "Could not draw line due to missing points");
    						return null;
    					}
    					arc[] newLine = new arc[list.size()-1];
    					for(int i=0; i<newLine.length;i++) {
    						newLine[i] = new arc(list.get(i),list.get(i+1));
    					}
    					if(newLine.length>0) {
    						temp = new ColorLine(newLine,newElement.getOptions().getColor(),newElement.getOptions().getStroakThicknes(),newElement.getOptions().getLayer());
//    				    	mapReference.put(newElement, temp);
//    				    	figuresToBeAdded.add(temp);
    					}
    					
    				}else if(newElement.getClass()==pointDrawer.class) {
    					ArrayList<point> list = newElement.getOptions().getPointList();
    			    	temp = new ColorPoint(list.get(0),newElement.getOptions().getColor(),newElement.getOptions().getSize(),newElement.getOptions().getLayer());
//    			    	mapReference.put(newElement, temp);
//    			    	figuresToBeAdded.add(temp);
    					
    				}else if(newElement.getClass()==texture2DDrawer.class) {
    			    	temp = new TractorAddtional(context, newElement.getOptions().getSize(), newElement.getOptions().getTextureResource2D(), perspective,newElement.getOptions().getLayer(),newElement.getOptions().getPosition(),newElement.getOptions().getRotation(),newElement.getOptions().getAnchorX2d(),newElement.getOptions().getAnchorY2d());
//    			    	mapReference.put(newElement, temp);
//    			    	figuresToBeAdded.add(temp);

    				}else if(newElement.getClass()==texture2DDrawerCombine.class) {
    			    	temp = new Combine2D(context, newElement.getOptions().getSize(), newElement.getOptions().getTextureResource2D(), perspective,newElement.getOptions().getLayer(),newElement.getOptions().getPosition(),newElement.getOptions().getRotation(),newElement.getOptions().getAnchorX2d(),newElement.getOptions().getAnchorY2d(),
    			    			newElement.getOptions().getTextureResource2D_2(),newElement.getOptions().getTextureResource2D_3(),newElement.getOptions().isUseSpout());
//    			    	mapReference.put(newElement, temp);
//    			    	figuresToBeAdded.add(temp);

    				}else if(newElement.getClass()==texture3DDrawer.class) {
    					temp = new Tractor(context, newElement.getOptions().getSize(),newElement.getOptions().getTextureResource2D(),newElement.getOptions().getTextureResource3D(),perspective,newElement.getOptions().getLayer(),
    							newElement.getOptions().getAnchorX2d(), newElement.getOptions().getAnchorY2d(), newElement.getOptions().getAnchorX3d(), newElement.getOptions().getAnchorY3d());
//    					mapReference.put(newElement, temp);
//    					figuresToBeAdded.add(temp);
    				}else if(newElement.getClass()==mapDrawer.class) {
    					point tempPoint = newElement.getOptions().getPointsB().get(0).coordinateToPoint();
    					
    					temp = new mapKortforsyning(context, (int) tempPoint.x,(int) tempPoint.y,perspective,newElement.getOptions().originOffset(),newElement.getOptions().getSize());
//    					mapReference.put(newElement, temp);
//    					figuresToBeAdded.add(temp);
    				}else if(newElement.getClass()==multiPolygonDrawer.class){
    					ArrayList<ArrayList<point>> list = newElement.getOptions().getPointListMulti();
    					polygon[] newPolygons = new polygon[list.size()];
    					for(int i=0;i<newPolygons.length;i++){
    						point[] points = new point[list.get(i).size()];
    						list.get(i).toArray(points);
    						newPolygons[i] = new polygon(points);
    					}
    					polygon[] edges = null;
    					if(newElement.getOptions().getUseEdge()){
    						list = newElement.getOptions().getPointListEdgeMulti();
    						edges = new polygon[list.size()];
        					for(int i=0;i<edges.length;i++){
        						point[] points = new point[list.get(i).size()];
        						list.get(i).toArray(points);
        						edges[i] = new polygon(points);
        					}
    					}
    					temp = new ColorMultiPoly(newPolygons, edges, newElement.getOptions().getColor(), newElement.getOptions().getStroakThicknes(), newElement.getOptions().getStroakColor(), newElement.getOptions().getLayer());
    				}else if(newElement.getClass()==texture3DDrawerCombine.class){
    					temp = new Combine3D(context, newElement.getOptions().getSize(),newElement.getOptions().getTextureResource2D(),newElement.getOptions().getTextureResource3D(),perspective,newElement.getOptions().getLayer(),
    							newElement.getOptions().getAnchorX2d(), newElement.getOptions().getAnchorY2d(), newElement.getOptions().getAnchorX3d(), newElement.getOptions().getAnchorY3d(), 
    							newElement.getOptions().getTextureResource2D_2(), newElement.getOptions().getTextureResource3D_2(), newElement.getOptions().getTextureResource2D_3(), 
    							newElement.getOptions().getTextureResource3D_3(), newElement.getOptions().isUseSpout());
    				}else if(newElement.getClass()==texture2DDrawerTrailer.class){
    					temp = new Trailer2D(context, newElement.getOptions().getSize(), newElement.getOptions().getTextureResource2D(), perspective,newElement.getOptions().getLayer(),newElement.getOptions().getPosition(),newElement.getOptions().getRotation(),newElement.getOptions().getAnchorX2d(),newElement.getOptions().getAnchorY2d(),
    			    			newElement.getOptions().getTextureResource2D_2());
    				}else if(newElement.getClass()==texture3DDrawerTrailer.class){
    					temp   = new Trailer3D(context, newElement.getOptions().getSize(),newElement.getOptions().getTextureResource2D(),newElement.getOptions().getTextureResource3D(),perspective,newElement.getOptions().getLayer(),
    							newElement.getOptions().getAnchorX2d(), newElement.getOptions().getAnchorY2d(), newElement.getOptions().getAnchorX3d(), newElement.getOptions().getAnchorY3d(), 
    							newElement.getOptions().getTextureResource2D_2());
    				}
    				newElement.drawer = temp;
    				updateObjects(newElement, temp);    				
    				}catch(Exception e){
    					Log.e("ERROR",e.getLocalizedMessage(),e);
    				}
					return null;
    			}
    		}.executeOnExecutor(AIExecutor,null, null, null);
    		boolean taskStarted = as.getStatus()==AsyncTask.Status.RUNNING || as.getStatus()==AsyncTask.Status.FINISHED;
//    		if(!taskStarted) {
//    			elementsToBeAdded.add(newElement);
//    		}
    		
    	return null;
    }

    public synchronized void updateObjects(mapElement newElement, basicDrawer temp) {
    	if(temp!=null) {
    		if(!mapReference.containsKey(newElement)){
    			mapReference.put(newElement, temp);
    			figuresToBeAdded.add(temp);
    			added = true;
    		}
    	}
    }

    public synchronized void updatePerspective() {
    	if(camera!=null) {
    		if(camera.tilt!=0) {
    			this.perspective.setAngle3d(camera.tilt);
    			this.perspective.setView3d(true);
    		}else {
    			this.perspective.setView3d(false);
    		}
    		this.perspective.setFieldTranslation(camera.position);
    		this.perspective.setRotation(camera.bearing);
    		this.perspective.setZoom(camera.getZoom());
    		this.perspective.setFieldFree(camera.positionMovement);
    		this.perspective.setFieldCenter(camera.fieldCenter);
    		camera.positionMovement = new float[]{0,0};
    		updateProjections();
    	}
    }
    public void remove(mapElement newElement) {
    	if(!elementsToBeRemoved.contains(newElement)){
    		Log.i("AIMap", "Removing object: " + newElement.hashCode());
    		this.elementsToBeRemoved.add(newElement);
    		removed = true;
    	}
    }
    public void removeFigures() {
    	ArrayList<mapElement> temp = new ArrayList<mapElement>();
    	temp.addAll(elementsToBeRemoved);
    	for(int i=0;i<temp.size();i++) {
    		if(mapReference.containsKey(temp.get(i))) {
    			for(int j=0;j<figures.size();j++) {
    				if(this.figures.get(j).contains(mapReference.get(temp.get(i)))) {
    					this.figures.get(j).remove(mapReference.get(temp.get(i)));
    					break;
    				}
    			}
    			this.figuresToBeInFront.remove(mapReference.get(temp.get(i)));
    			elementsToBeRemoved.remove(temp.get(i));
    			mapReference.remove(temp.get(i));
    			temp.get(i).removed = true;
    		}
    	}
    }

    
    public void addFigures() {
    	added = false;
    	ArrayList<basicDrawer> temp = new ArrayList<basicDrawer>();
    	temp.addAll(figuresToBeAdded);
    	for(int i=0;i<temp.size();i++) {
    		while(this.figures.size()<temp.get(i).layer+1) {
    			this.figures.add(new ArrayList<basicDrawer>());
    		}
    		this.figures.get(temp.get(i).layer).add(temp.get(i));
    		figuresToBeAdded.remove(temp.get(i));
    	}
    	figuresToBeUpdated.addAll(temp);
    }
    public void replaceFigures() {
    	added = false;
    	ArrayList<basicDrawer> temp = new ArrayList<basicDrawer>();
    	temp.addAll(figuresToBeAdded);
    	for(int i=0;i<temp.size();i++) {
    		while(this.figures.size()<temp.get(i).layer+1) {
    			this.figures.add(new ArrayList<basicDrawer>());
    		}
    		this.figures.get(temp.get(i).layer).add(temp.get(i));
    		figuresToBeAdded.remove(temp.get(i));
    	}
    	figuresToBeUpdated.addAll(temp);
    }
    public synchronized void replace(final mapElement replaceElement){
    	
    	Log.i("AIMap", "Trying to Replace graphic "+AIExecutor.toString());
    	AsyncTask<Void, Void, Void> as = new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... params) {
				try{
					if(!mapReference.containsKey(replaceElement)){
						Log.i("OPENGL", "Trying to replace null object, adding instead");
						replaceElement.drawOnMap();
						return null;
					}
					if(replaceElement.getClass()==polygonDrawer.class) {
						ArrayList<point> list = replaceElement.getOptions().getPointList();
						point[] points = new point[list.size()];
						list.toArray(points);
						polygon newPolygon = new polygon(points);
						ColorPoly drawer = (ColorPoly) mapReference.get(replaceElement);
						polygon edge = null;
						if(replaceElement.getOptions().getUseEdge()){
							list = replaceElement.getOptions().getPointListEdge();
	    					points = new point[list.size()];
	    					list.toArray(points);
							edge = new polygon(points);
						}
						if(drawer!=null){
							drawer.replace(newPolygon,edge,replaceElement.getOptions().getColor(),replaceElement.getOptions().getStroakThicknes(),replaceElement.getOptions().getStroakColor(),replaceElement.getOptions().getLayer());
						}
					}else if(replaceElement.getClass()==lineDrawer.class) {
						ArrayList<point> list = replaceElement.getOptions().getPointList();
						if(list.size()==0) {
							Log.i("OPENGL", "Could not draw line due to missing points");
							return null;
						}
						arc[] newLine = new arc[list.size()-1];
						for(int i=0; i<newLine.length;i++) {
							newLine[i] = new arc(list.get(i),list.get(i+1));
						}
						if(newLine.length>0) {
							ColorLine drawer = (ColorLine) mapReference.get(replaceElement);
							if(drawer!=null){
								drawer.replace(newLine,replaceElement.getOptions().getColor(),replaceElement.getOptions().getStroakThicknes(),replaceElement.getOptions().getLayer());
							}
						}
						
					}else if(replaceElement.getClass()==pointDrawer.class) {
						ColorPoint drawer = (ColorPoint) mapReference.get(replaceElement);
						ArrayList<point> list = replaceElement.getOptions().getPointList();
						if(drawer!=null){
							drawer.replace(list.get(0),replaceElement.getOptions().getColor(),replaceElement.getOptions().getSize(),replaceElement.getOptions().getLayer());
						}
					}else if(replaceElement.getClass()==texture2DDrawer.class) {

					}else if(replaceElement.getClass()==texture3DDrawer.class) {

					}else if(replaceElement.getClass()==mapDrawer.class) {
						
					}
					return null;
				}catch(Exception e){
					Log.e("ERROR",e.getLocalizedMessage(),e);
				}
				return null;
				
			}


		}.executeOnExecutor(AIExecutor,null, null, null);
		
    }
    public void moveFigures() {
    	ArrayList<mapElement> temp = new ArrayList<mapElement>();
    	temp.addAll(elementsToBeMoved);
    	for(int i=0;i<temp.size();i++) {
    		if(mapReference.containsKey(temp.get(i))) {
    			for(int j=0;j<figures.size();j++) {
    				if(this.figures.get(j).contains(mapReference.get(temp.get(i)))) {
    					this.figures.get(j).remove(mapReference.get(temp.get(i)));
    	    			this.figuresToBeInFront.add(0,mapReference.get(temp.get(i)));
    	    			elementsToBeMoved.remove(temp.get(i));
    	    			break;
    				}
    			}
    		}
    	}
    }

}
