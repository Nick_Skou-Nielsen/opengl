package com.opengl.android.Geometric;

import static android.opengl.GLES20.*;

import geometricObjects.point;

public class ColorPoint extends ColorPoly {
	public ColorPoint(final point point,final float[] color,final double size,final int layer) {
		this.polygon = GeometricTransformation.pointToVertexArray(point, color, size);
    	this.vertexArray = new VertexArray(polygon);
    	this.layer=layer;
	}
	public void replace(final point point,final float[] color,final double size,final int layer){
		this.polygon = GeometricTransformation.pointToVertexArray(point, color, size);
    	this.vertexArray = new VertexArray(polygon);
    	this.layer=layer;
	}
	public synchronized void draw() {
    	if (program!=null) {
    		program.useProgram();
        	program.setUniforms();
        	bindData();
        	glDrawArrays(GL_TRIANGLE_FAN, 0, polygon.length/(POSITION_COMPONENT_COUNT+COLOR_COMPONENT_COUNT));
    	}
	}
}
