package com.opengl.android.Geometric;

import static android.opengl.GLES20.GL_TRIANGLE_FAN;
import static android.opengl.GLES20.glDrawArrays;

import java.io.IOException;
import java.net.MalformedURLException;

import com.opengl.android.Engines.TextureHelper;
import com.opengl.android.Engines.TextureShaderProgram;
import com.opengl.android.Util.Constants;
import com.opengl.android.Util.PerspectiveHandler;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import geometricObjects.point;

public class mapKortforsyning extends TexturePoly {
	private int texture2dSat;
	private int texture2dBasis;
	private final String URL1;
	private final String URL2;
	private final float[] position;
	private TextureShaderProgram textureProgram;
	private final VertexArray vertexArray2d;
	private final PerspectiveHandler perspektive;
	private float[] Tractor2d = {
			// Order of coordinates: X, Y, S, T
			// Triangle Fan
			0f, 0f, 0.5f, 0.5f,
			-0.5f, -0.5f, 0f, 1f,
			0.5f, -0.5f, 1f, 1f,
			0.5f, 0.5f, 1f, 0f,
			-0.5f, 0.5f, 0f, 0f,
			-0.5f, -0.5f, 0f, 1f };
	
	public void update() throws MalformedURLException, IOException {
		texture2dSat = TextureHelper.loadTextureURL(textureProgram.getContext(), URL1);
		texture2dBasis = TextureHelper.loadTextureURL(textureProgram.getContext(), URL2);
	}

	public mapKortforsyning(Context context,int x, int y, PerspectiveHandler perspektive, point offset, float size) {
		super();
		position = new float[] {(float) (x-offset.x),(float) (y-offset.y)};
		URL1 = "http://services.kortforsyningen.dk/service?SERVICENAME=forvaltning&login=AgroIntelli&password=ai_toro2017&service=WMS&version=1.1.1&request=GetMap&srs=EPSG:25832&bbox=" + (x-size/2) + "," + (y-size/2) + ","+ (x+size/2) + "," + (y+size/2) + "&width=320&height=320&LAYERS=basis_ortofoto&styles=&format=image/jpeg&jpegquality=80&styles=&exceptions=application/vnd.ogc.se_inimage";
		URL2 = "http://services.kortforsyningen.dk/service?SERVICENAME=forvaltning&login=AgroIntelli&password=ai_toro2017&service=WMS&version=1.1.1&request=GetMap&srs=EPSG:25832&bbox=" + (x-size/2) + "," + (y-size/2) + ","+ (x+size/2) + "," + (y+size/2) + "&width=320&height=320&LAYERS=basis_kort&styles=&format=image/jpeg&jpegquality=80&styles=&exceptions=application/vnd.ogc.se_inimage";
		this.layer = 0;
		BitmapFactory.Options dimensions = new BitmapFactory.Options(); 
		dimensions.inJustDecodeBounds = true;
			Tractor2d[5] = Tractor2d[5] *size + position[1];
			Tractor2d[9] = Tractor2d[9] *size + position[1];
			Tractor2d[13] = Tractor2d[13] *size + position[1];
			Tractor2d[17] = Tractor2d[17] *size + position[1];
			Tractor2d[21] = Tractor2d[21] *size + position[1];
			Tractor2d[4] = Tractor2d[4] * size + position[0];
			Tractor2d[8] = Tractor2d[8] * size + position[0];
			Tractor2d[12] = Tractor2d[12] * size + position[0];
			Tractor2d[16] = Tractor2d[16] * size + position[0];
			Tractor2d[20] = Tractor2d[20] * size + position[0];
			Tractor2d[0] = Tractor2d[0] + position[0];
			Tractor2d[1] = Tractor2d[1] + position[1];
		vertexArray2d = new VertexArray(Tractor2d);
		
		this.perspektive = perspektive;
	}
	public void setProgram(TextureShaderProgram program) {
		this.textureProgram = program;
	}
	
	public synchronized void draw() {
		if(texture2dSat==0 || texture2dBasis==0) {
			try {
				update();
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return;
			
		}
		if(textureProgram!=null && Constants.backGroundMap==true) {
			textureProgram.useProgram();
			if(Constants.satMap==true) {
				textureProgram.setUniforms(texture2dSat);
			}else if(Constants.basisMap==true){
				textureProgram.setUniforms(texture2dBasis);
			}
			bindData(textureProgram,perspektive.getView3d());
			glDrawArrays(GL_TRIANGLE_FAN, 0, Tractor2d.length/(POSITION_COMPONENT_COUNT+TEXTURE_COORDINATES_COMPONENT_COUNT));
		}
	}
	public void bindData(TextureShaderProgram textureProgram,boolean graphic3d) {
			vertexArray2d.setVertexAttribPointer(0,textureProgram.getPositionAttributeLocation(),POSITION_COMPONENT_COUNT,STRIDE);
			vertexArray2d.setVertexAttribPointer(POSITION_COMPONENT_COUNT,textureProgram.getTextureCoordinatesAttributeLocation(),TEXTURE_COORDINATES_COMPONENT_COUNT,STRIDE);	
	}
}
