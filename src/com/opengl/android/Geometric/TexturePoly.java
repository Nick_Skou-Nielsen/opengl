package com.opengl.android.Geometric;
import static com.opengl.android.Util.Constants.BYTES_PER_FLOAT;

import com.opengl.android.Engines.ShaderProgram;
import com.opengl.android.Engines.TextureShaderProgram;

import static android.opengl.GLES20.*;


public class TexturePoly extends basicDrawer{
	protected static final int POSITION_COMPONENT_COUNT = 2;
	protected static final int TEXTURE_COORDINATES_COMPONENT_COUNT = 2;
	protected static final int STRIDE = (POSITION_COMPONENT_COUNT + TEXTURE_COORDINATES_COMPONENT_COUNT) * BYTES_PER_FLOAT;
	protected final VertexArray vertexArray;
	protected final float[] VERTEX_DATA;
	
	protected TexturePoly() {
		VERTEX_DATA = null;
		vertexArray = null;
	}
	
	public TexturePoly(float[] VERTEX_DATA) {
		this.VERTEX_DATA = VERTEX_DATA;
		vertexArray = new VertexArray(VERTEX_DATA);
	}
	public void bindData(ShaderProgram textureProgram) {
		vertexArray.setVertexAttribPointer(0,((TextureShaderProgram)textureProgram).getPositionAttributeLocation(),POSITION_COMPONENT_COUNT,STRIDE);
		vertexArray.setVertexAttribPointer(POSITION_COMPONENT_COUNT,((TextureShaderProgram)textureProgram).getTextureCoordinatesAttributeLocation(),TEXTURE_COORDINATES_COMPONENT_COUNT,STRIDE);
	}
	public synchronized void draw() {
		glDrawArrays(GL_TRIANGLE_FAN, 0, VERTEX_DATA.length/(POSITION_COMPONENT_COUNT+TEXTURE_COORDINATES_COMPONENT_COUNT));
	}
}
