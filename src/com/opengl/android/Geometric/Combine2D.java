package com.opengl.android.Geometric;

import static android.opengl.GLES20.GL_TRIANGLE_FAN;
import static android.opengl.GLES20.glDrawArrays;

import com.opengl.android.R;
import com.opengl.android.Engines.TextureHelper;
import com.opengl.android.Engines.TextureShaderProgram;
import com.opengl.android.Util.PerspectiveHandler;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class Combine2D extends TexturePoly {
	private int spoutTexture2dOn;
	private int spoutTexture2dOff;
	private int spout2dOn;
	private int spout2dOff;
	private boolean spoutOn = false;
	private int texture2d;
	private final int tractor2d;
	private final float[] projectionMatrix = new float[16];
	private float rotation = 0;
	private int height2d;
	private int width2d;
	private float anochorY2D;
	private float anochorX2D;
	private float size;
	private float[] location = new float[] {0f,0f};
	private TextureShaderProgram textureProgram;
	private PerspectiveHandler perspektive;
	private VertexArray vertexArray2d;
	private float[] Tractor2d = {
			// Order of coordinates: X, Y, S, T
			// Triangle Fan
			0f, 0f, 0.5f, 0.5f,
			-0.5f, -0.5f, 0f, 1f,
			0.5f, -0.5f, 1f, 1f,
			0.5f, 0.5f, 1f, 0f,
			-0.5f, 0.5f, 0f, 0f,
			-0.5f, -0.5f, 0f, 1f };
	
	public void setLocation(final float[] location) {
		this.location = location;
		this.perspektive.setLocation(this, location);
	}
	public void setRotation(final float rotation) {
		this.rotation = -rotation;
	}
	public void setSize(float size) {
		Tractor2d = new float[] {
				// Order of coordinates: X, Y, S, T
				// Triangle Fan
				0f, 0f, 0.5f, 0.5f,
				-0.5f, -0.5f, 0f, 1f,
				0.5f, -0.5f, 1f, 1f,
				0.5f, 0.5f, 1f, 0f,
				-0.5f, 0.5f, 0f, 0f,
				-0.5f, -0.5f, 0f, 1f };

		if(height2d>width2d) {
			Tractor2d[5] = Tractor2d[5] * height2d/width2d*size+(anochorY2D-0.5f)* height2d/width2d*size;
			Tractor2d[9] = Tractor2d[9] * height2d/width2d*size+(anochorY2D-0.5f)* height2d/width2d*size;
			Tractor2d[13] = Tractor2d[13] * height2d/width2d*size+(anochorY2D-0.5f)* height2d/width2d*size;
			Tractor2d[17] = Tractor2d[17] * height2d/width2d*size+(anochorY2D-0.5f)* height2d/width2d*size;
			Tractor2d[21] = Tractor2d[21] * height2d/width2d*size+(anochorY2D-0.5f)* height2d/width2d*size;
			Tractor2d[4] = Tractor2d[4] * size+(anochorX2D-0.5f)*size;
			Tractor2d[8] = Tractor2d[8] * size+(anochorX2D-0.5f)*size;
			Tractor2d[12] = Tractor2d[12] * size+(anochorX2D-0.5f)*size;
			Tractor2d[16] = Tractor2d[16] * size+(anochorX2D-0.5f)*size;
			Tractor2d[20] = Tractor2d[20] * size+(anochorX2D-0.5f)*size;
			Tractor2d[0] = Tractor2d[0] +(anochorX2D-0.5f)*size;
			Tractor2d[1] = Tractor2d[1] +(anochorY2D-0.5f)* height2d/width2d*size;
		}else {
			Tractor2d[4] = Tractor2d[4] * width2d/height2d*size+(anochorX2D-0.5f)* height2d/width2d*size;
			Tractor2d[8] = Tractor2d[8] * width2d/height2d*size+(anochorX2D-0.5f)* height2d/width2d*size;
			Tractor2d[12] = Tractor2d[12] * width2d/height2d*size+(anochorX2D-0.5f)* height2d/width2d*size;
			Tractor2d[16] = Tractor2d[16] * width2d/height2d*size+(anochorX2D-0.5f)* height2d/width2d*size;
			Tractor2d[20] = Tractor2d[20] * width2d/height2d*size+(anochorX2D-0.5f)* height2d/width2d*size;
			Tractor2d[5] = Tractor2d[5] * size+(anochorY2D-0.5f)*size;
			Tractor2d[9] = Tractor2d[9] * size+(anochorY2D-0.5f)*size;
			Tractor2d[13] = Tractor2d[13] * size+(anochorY2D-0.5f)*size;
			Tractor2d[17] = Tractor2d[17] * size+(anochorY2D-0.5f)*size;
			Tractor2d[21] = Tractor2d[21] * size+(anochorY2D-0.5f)*size;
			Tractor2d[1] = Tractor2d[1] +(anochorY2D-0.5f)*size;
			Tractor2d[0] = Tractor2d[0] +(anochorX2D-0.5f)* height2d/width2d*size;
		}
		vertexArray2d = new VertexArray(Tractor2d);
	}
	public void update() {
		texture2d = TextureHelper.loadTexture(textureProgram.getContext(), tractor2d);
		spoutTexture2dOn = TextureHelper.loadTexture(textureProgram.getContext(), spout2dOn);
		spoutTexture2dOff = TextureHelper.loadTexture(textureProgram.getContext(), spout2dOff);

	}
	public void useSpout(boolean useSpout){
		this.spoutOn = useSpout;
	}
	public Combine2D(final Context context,final float size,final int tractor2d,
			final PerspectiveHandler perspektive,final int layer, 
			final float[] position,final float rotation,final float anochorX2D,final float anochorY2D,
			final int spout2dOn,final int spout2dOff,final boolean spoutOn) {
		super();
		this.spout2dOn = spout2dOn;
		this.spout2dOff = spout2dOff;
		this.spoutOn = spoutOn;
		this.tractor2d = tractor2d;
		this.layer = layer;
		this.size=size;
		BitmapFactory.Options dimensions = new BitmapFactory.Options(); 
		dimensions.inJustDecodeBounds = true;
		Bitmap mBitmap = BitmapFactory.decodeResource(context.getResources(), tractor2d, dimensions);
		height2d = dimensions.outHeight;
		width2d =  dimensions.outWidth;
		this.anochorX2D = anochorX2D;
		this.anochorY2D = anochorY2D;
		if(height2d>width2d) {
			Tractor2d[5] = Tractor2d[5] * height2d/width2d*size+(anochorY2D-0.5f)* height2d/width2d*size;
			Tractor2d[9] = Tractor2d[9] * height2d/width2d*size+(anochorY2D-0.5f)* height2d/width2d*size;
			Tractor2d[13] = Tractor2d[13] * height2d/width2d*size+(anochorY2D-0.5f)* height2d/width2d*size;
			Tractor2d[17] = Tractor2d[17] * height2d/width2d*size+(anochorY2D-0.5f)* height2d/width2d*size;
			Tractor2d[21] = Tractor2d[21] * height2d/width2d*size+(anochorY2D-0.5f)* height2d/width2d*size;
			Tractor2d[4] = Tractor2d[4] * size+(anochorX2D-0.5f)*size;
			Tractor2d[8] = Tractor2d[8] * size+(anochorX2D-0.5f)*size;
			Tractor2d[12] = Tractor2d[12] * size+(anochorX2D-0.5f)*size;
			Tractor2d[16] = Tractor2d[16] * size+(anochorX2D-0.5f)*size;
			Tractor2d[20] = Tractor2d[20] * size+(anochorX2D-0.5f)*size;
			Tractor2d[0] = Tractor2d[0] +(anochorX2D-0.5f)*size;
			Tractor2d[1] = Tractor2d[1] +(anochorY2D-0.5f)* height2d/width2d*size;
		}else {
			Tractor2d[4] = Tractor2d[4] * width2d/height2d*size+(anochorX2D-0.5f)* height2d/width2d*size;
			Tractor2d[8] = Tractor2d[8] * width2d/height2d*size+(anochorX2D-0.5f)* height2d/width2d*size;
			Tractor2d[12] = Tractor2d[12] * width2d/height2d*size+(anochorX2D-0.5f)* height2d/width2d*size;
			Tractor2d[16] = Tractor2d[16] * width2d/height2d*size+(anochorX2D-0.5f)* height2d/width2d*size;
			Tractor2d[20] = Tractor2d[20] * width2d/height2d*size+(anochorX2D-0.5f)* height2d/width2d*size;
			Tractor2d[5] = Tractor2d[5] * size+(anochorY2D-0.5f)*size;
			Tractor2d[9] = Tractor2d[9] * size+(anochorY2D-0.5f)*size;
			Tractor2d[13] = Tractor2d[13] * size+(anochorY2D-0.5f)*size;
			Tractor2d[17] = Tractor2d[17] * size+(anochorY2D-0.5f)*size;
			Tractor2d[21] = Tractor2d[21] * size+(anochorY2D-0.5f)*size;
			Tractor2d[1] = Tractor2d[1] +(anochorY2D-0.5f)*size;
			Tractor2d[0] = Tractor2d[0] +(anochorX2D-0.5f)* height2d/width2d*size;
		}
		vertexArray2d = new VertexArray(Tractor2d);
		this.location = position;
		this.rotation = rotation;
		texture2d = TextureHelper.loadTexture(context, tractor2d);
		this.perspektive = perspektive;
	}
	public synchronized void draw() {
		if(textureProgram!=null) {
			this.setSize((float) (size*-Math.pow(2, perspektive.getZoom())/Math.max(Math.min((float) -Math.pow(1.7, perspektive.getZoom()+1),(float) -Math.pow(2, 5)),(float) -Math.pow(2, 6))));
			perspektive.getProjectionMatrixAddtionalTractors(projectionMatrix, rotation, location);
			textureProgram.useProgram();
			textureProgram.setUniforms(projectionMatrix, texture2d);
			bindData(textureProgram);
			glDrawArrays(GL_TRIANGLE_FAN, 0, Tractor2d.length/(POSITION_COMPONENT_COUNT+TEXTURE_COORDINATES_COMPONENT_COUNT));
			if(spoutOn){
				textureProgram.setUniforms(projectionMatrix, spoutTexture2dOn);
				bindData(textureProgram);
			}else{
				textureProgram.setUniforms(projectionMatrix, spoutTexture2dOff);
				bindData(textureProgram);
			}
			glDrawArrays(GL_TRIANGLE_FAN, 0, Tractor2d.length/(POSITION_COMPONENT_COUNT+TEXTURE_COORDINATES_COMPONENT_COUNT));
		}
	}
	public void bindData(TextureShaderProgram textureProgram) {
		vertexArray2d.setVertexAttribPointer(0,textureProgram.getPositionAttributeLocation(),POSITION_COMPONENT_COUNT,STRIDE);
		vertexArray2d.setVertexAttribPointer(POSITION_COMPONENT_COUNT,textureProgram.getTextureCoordinatesAttributeLocation(),TEXTURE_COORDINATES_COMPONENT_COUNT,STRIDE);
	}
	public void setProgram(TextureShaderProgram program) {
		this.textureProgram = program;
	}
}
