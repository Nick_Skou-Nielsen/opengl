package com.opengl.android.Geometric;

import static android.opengl.GLES20.GL_TRIANGLE_FAN;
import static android.opengl.GLES20.glDrawArrays;

import com.opengl.android.R;
import com.opengl.android.Engines.ColorShaderProgram;
import com.opengl.android.Engines.TextureHelper;
import com.opengl.android.Engines.TextureShaderProgram;
import com.opengl.android.Util.PerspectiveHandler;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import geometricObjects.direction;

public class Trailer2D extends TexturePoly {
	private int texture2d;
	private int trailer2d;
	private final int tractor2d;
	private int trailerTexture2d;
	private final float[] projectionMatrix = new float[16];
	private float rotation = 0;
	private float rotationTrailer = 0;
	private int height2d;
	private int width2d;
	private int height2dTrailer;
	private int width2dTrailer;

	private float anochorY2D;
	private float anochorX2D;
	private float size;
	private float[] location = new float[] {0f,0f};
	private TextureShaderProgram textureProgram;
	private ColorShaderProgram colorShaderProgram;
	private PerspectiveHandler perspektive;
	private VertexArray vertexArray2d;
	private VertexArray vertexArray2dTrailer;
	private float load = 0;

	private float[] Tractor2d;
	private float[] Trailer2d;

	
	public void setLocation(final float[] location) {
		this.location = location;
	}
	
	public void setLoad(final float load) {
		this.load = load;
	}
	
	public void setRotation(final float rotation) {
		direction oldDir = new direction(this.rotation);
		direction newDir = new direction(-rotation);
		double diffDir = Math.abs(oldDir.angleDifference(newDir));
		if(diffDir>45){
			direction temp1 = newDir.rotate(Math.PI/2);
			direction temp2 = newDir.rotate(-Math.PI/2);
			if(Math.abs(oldDir.angleDifference(temp1))>Math.abs(oldDir.angleDifference(temp2))){
				this.rotationTrailer = (float) temp2.getBearing();
			}else{
				this.rotationTrailer = (float) temp1.getBearing();
			}
		}else{
			this.rotationTrailer = this.rotation;
		}
		this.rotation = -rotation;
	}
	
	public void setSize(final float size) {
		Tractor2d = GeometricTransformation.makeTextureSquare(size, anochorX2D, 1f, height2d, width2d);
		Trailer2d = GeometricTransformation.makeTextureSquare(size, anochorX2D, 0.1f, height2dTrailer, width2dTrailer);

		vertexArray2d = new VertexArray(Tractor2d);
		vertexArray2dTrailer = new VertexArray(Trailer2d);
	}
	
	public void update() {
		texture2d = TextureHelper.loadTexture(textureProgram.getContext(), tractor2d);
		trailerTexture2d = TextureHelper.loadTexture(textureProgram.getContext(), trailer2d);
	}
	
	public Trailer2D(final Context context,final float size,final int tractor2d,
			final PerspectiveHandler perspektive,final int layer, 
			final float[] position,final float rotation,final float anochorX2D,final float anochorY2D,
			final int trailer2d) {
		super();
		this.trailer2d=trailer2d;
		this.tractor2d = tractor2d;
		this.layer = layer;
		this.size=size;
		this.rotationTrailer = -rotation;
		BitmapFactory.Options dimensions = new BitmapFactory.Options(); 
		dimensions.inJustDecodeBounds = true;
		Bitmap mBitmap = BitmapFactory.decodeResource(context.getResources(), tractor2d, dimensions);
		height2d = dimensions.outHeight;
		width2d =  dimensions.outWidth;
		this.anochorX2D = anochorX2D;
		this.anochorY2D = anochorY2D;
		
		mBitmap = BitmapFactory.decodeResource(context.getResources(), trailer2d, dimensions);
		height2dTrailer = dimensions.outHeight;
		width2dTrailer =  dimensions.outWidth;
		
		Tractor2d = GeometricTransformation.makeTextureSquare(size, anochorX2D, 1f, height2d, width2d);
		
		Trailer2d = GeometricTransformation.makeTextureSquare(size, anochorX2D, 0.1f, height2dTrailer, width2dTrailer);

		vertexArray2d = new VertexArray(Tractor2d);
		vertexArray2dTrailer = new VertexArray(Trailer2d);
		
		this.location = position;
		this.rotation = -rotation;
		texture2d = TextureHelper.loadTexture(context, tractor2d);
		this.perspektive = perspektive;
	}
	
	public synchronized void draw() {
		if(textureProgram!=null) {
			float  tempSize = (float) (size*-Math.pow(2, perspektive.getZoom())/Math.max(Math.min((float) -Math.pow(1.7, perspektive.getZoom()+1),(float) -Math.pow(2, 5)),(float) -Math.pow(2, 6)));
			this.setSize(tempSize);
//			float[] trailerLoc = new float[]{location[0]-(float) (Math.sin(rotation)*tempSize*(1-anochorY2D)),location[1]-(float) (Math.cos(rotation)*tempSize*(1-anochorY2D))};
			perspektive.getProjectionMatrixAddtionalTractors(projectionMatrix, rotation, location);
			textureProgram.useProgram();
			textureProgram.setUniforms(projectionMatrix, texture2d);
			bindData2(textureProgram);
			glDrawArrays(GL_TRIANGLE_FAN, 0, Tractor2d.length/(POSITION_COMPONENT_COUNT+TEXTURE_COORDINATES_COMPONENT_COUNT));
			perspektive.getProjectionMatrixAddtionalTractors(projectionMatrix, rotationTrailer, location);
			textureProgram.setUniforms(projectionMatrix, trailerTexture2d);
			bindData(textureProgram);
			glDrawArrays(GL_TRIANGLE_FAN, 0, Tractor2d.length/(POSITION_COMPONENT_COUNT+TEXTURE_COORDINATES_COMPONENT_COUNT));
		}
	}
	
	public void bindData(TextureShaderProgram textureProgram) {
		vertexArray2dTrailer.setVertexAttribPointer(0,textureProgram.getPositionAttributeLocation(),POSITION_COMPONENT_COUNT,STRIDE);
		vertexArray2dTrailer.setVertexAttribPointer(POSITION_COMPONENT_COUNT,textureProgram.getTextureCoordinatesAttributeLocation(),TEXTURE_COORDINATES_COMPONENT_COUNT,STRIDE);
	}
	public void bindData2(TextureShaderProgram textureProgram) {
		vertexArray2d.setVertexAttribPointer(0,textureProgram.getPositionAttributeLocation(),POSITION_COMPONENT_COUNT,STRIDE);
		vertexArray2d.setVertexAttribPointer(POSITION_COMPONENT_COUNT,textureProgram.getTextureCoordinatesAttributeLocation(),TEXTURE_COORDINATES_COMPONENT_COUNT,STRIDE);
	}
	
	public void setProgram(TextureShaderProgram program) {
		this.textureProgram = program;
	}
	public void setProgram2(ColorShaderProgram program) {
		this.colorShaderProgram = program;
	}
	
}
