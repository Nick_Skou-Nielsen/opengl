package com.opengl.android.Geometric;

import static android.opengl.GLES20.GL_TRIANGLES;
import static android.opengl.GLES20.glDrawArrays;

import com.opengl.android.Engines.ColorShaderProgram;

import geometricObjects.arc;
import geometricObjects.point;

public class ColorLine extends ColorPoly {
	private ColorPoint[] points;
	public ColorLine(final arc[] line,final float[] color,final double thickness,final int layer) {
		this.polygon = GeometricTransformation.arcToVertexArray(line,color, thickness);
    	this.vertexArray = new VertexArray(polygon);
    	this.layer = layer;
//    	points = new ColorPoint[line.length+1];
//    	for(int i=0;i<line.length;i++) {
//    		points[i] = new ColorPoint(line[i].startPoint, color, thickness);
//    	}
//    	points[line.length] = new ColorPoint(line[line.length-1].pointOnArc(1), color, thickness);
	}
	public synchronized void replace(final arc[] line,final float[] color,final double thickness,final int layer){
		this.polygon = GeometricTransformation.arcToVertexArray(line,color,thickness);
    	this.vertexArray = new VertexArray(polygon);
    	this.layer = layer;
	}
	public void setProgram(ColorShaderProgram program) {
    	this.program = program;
//    	for(int i=0;i<points.length;i++) {
//    		points[i].setProgram(program);
//    	}
    }
	public synchronized void draw() {
    	if (program!=null) {
    		program.useProgram();
        	program.setUniforms();
        	bindData();
        	glDrawArrays(GL_TRIANGLES, 0, polygon.length/(POSITION_COMPONENT_COUNT+COLOR_COMPONENT_COUNT));
//        	for(int i=0;i<points.length;i++) {
//        		points[i].draw();
//        	}
    	}
	}
}
