package com.opengl.android.Geometric;

import static android.opengl.GLES20.GL_TRIANGLE_FAN;
import static android.opengl.GLES20.glDrawArrays;

import com.opengl.android.R;
import com.opengl.android.Engines.TextureHelper;
import com.opengl.android.Engines.TextureShaderProgram;
import com.opengl.android.Util.Constants;
import com.opengl.android.Util.PerspectiveHandler;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import geometricObjects.direction;
import geometricObjects.point;

public class Trailer3D extends TexturePoly {
	private int tractor2d;
	private int trailer2d;

	private int tractor3d;

	
	private int texture2d;
	private int textureArrow;
	private int texture3d;
	private int textureTrailer2d;

	private float anochorX2D;
	private float anochorX3D;
	private float anochorY2D;
	private float anochorY3D;
	private float height2d;
	private float height3d;
	private float width2d;
	private float width3d;
	private float trailerHeight;
	private float trailerWidth;
	private final float[] projectionMatrixTractor = new float[16];
	private TextureShaderProgram textureProgram;
	private VertexArray vertexArray2d;
	private VertexArray vertexArray3d;
	private VertexArray vertexArray2dTrailer;
	private VertexArray vertexArray2dArrow;
	private final PerspectiveHandler perspektive;
	private float[] Tractor2d;
	private float[] Tractor3d;
	private float[] Trailer2d;
	private float[] Arrow2d;
	private float originalSize;

		
	public void update() {
		texture2d = TextureHelper.loadTexture(textureProgram.getContext(), tractor2d);
		textureArrow = TextureHelper.loadTexture(textureProgram.getContext(), R.drawable.pil);
		texture3d = TextureHelper.loadTexture(textureProgram.getContext(), tractor3d);
		textureTrailer2d = TextureHelper.loadTexture(textureProgram.getContext(), trailer2d);
	}
	
	public void setSize(float size){
		Tractor2d = GeometricTransformation.makeTextureSquare(size, anochorX2D, 1f, height2d, width2d);
		Tractor3d = GeometricTransformation.makeTextureSquare(size, anochorX3D, anochorY3D, height3d, width3d);
		Trailer2d = GeometricTransformation.makeTextureSquare(size, anochorX2D, 0.1f, trailerHeight, trailerWidth);
		
		vertexArray2d = new VertexArray(Tractor2d);
		vertexArray3d = new VertexArray(Tractor3d);
		vertexArray2dTrailer = new VertexArray(Trailer2d);
	}

	public Trailer3D(Context context, float size, int tractor2d, int tractor3d,
			PerspectiveHandler perspektive, int layer, float anochorX2D, 
			float anochorY2D, float anochorX3D, float anochorY3D
			,int trailer2d) {
		super();
		originalSize = size;
		this.anochorY2D = anochorY2D;
		this.anochorX2D = anochorX2D;
		this.anochorY3D = anochorY3D;
		this.anochorX3D = anochorX3D;
		this.layer = layer;
		this.tractor2d = tractor2d;
		this.tractor3d = tractor3d;
		this.trailer2d = trailer2d;
		BitmapFactory.Options dimensions = new BitmapFactory.Options(); 
		dimensions.inJustDecodeBounds = true;
		Bitmap mBitmap = BitmapFactory.decodeResource(context.getResources(), tractor2d, dimensions);
		height2d = dimensions.outHeight;
		width2d =  dimensions.outWidth;
		Tractor2d = GeometricTransformation.makeTextureSquare(size, anochorX2D, 1f, height2d, width2d);
		vertexArray2d = new VertexArray(Tractor2d);
		mBitmap = BitmapFactory.decodeResource(context.getResources(), tractor3d, dimensions);
		height3d = dimensions.outHeight;
		width3d =  dimensions.outWidth;
		Tractor3d = GeometricTransformation.makeTextureSquare(size, anochorX3D, anochorY3D, height3d, width3d);
		vertexArray3d = new VertexArray(Tractor3d);
		
		mBitmap = BitmapFactory.decodeResource(context.getResources(), trailer2d, dimensions);
		trailerHeight = dimensions.outHeight;
		trailerWidth =  dimensions.outWidth;
		Trailer2d = GeometricTransformation.makeTextureSquare(size, anochorX2D, 0.1f, trailerHeight, trailerWidth);
		vertexArray2dTrailer = new VertexArray(Trailer2d);
		Arrow2d = GeometricTransformation.makeTextureSquare((float) (size*4.4),0.5f,0.5f,1,1);
		vertexArray2dArrow = new VertexArray(Arrow2d);
		this.perspektive = perspektive;
	}
	public void setProgram(TextureShaderProgram program) {
		this.textureProgram = program;
	}
	public synchronized void draw() {
		if(textureProgram!=null) {
			if(Constants.currentMapMode==Constants.mapModeVehicle || perspektive.getView3d()){
				this.setSize(originalSize);
				textureProgram.useProgram();
			perspektive.getProjectionMatrixTractor(projectionMatrixTractor);
			if(perspektive.getView3d()) {
				textureProgram.setUniforms(projectionMatrixTractor, texture3d);
			}else {
				textureProgram.setUniforms(projectionMatrixTractor, texture2d);
			}
			bindData(textureProgram,perspektive.getView3d());
			
			glDrawArrays(GL_TRIANGLE_FAN, 0, Tractor2d.length/(POSITION_COMPONENT_COUNT+TEXTURE_COORDINATES_COMPONENT_COUNT));
			if(!perspektive.getView3d()) {
					textureProgram.setUniforms(projectionMatrixTractor, textureTrailer2d);
					bindData2(textureProgram);
					glDrawArrays(GL_TRIANGLE_FAN, 0, Tractor2d.length/(POSITION_COMPONENT_COUNT+TEXTURE_COORDINATES_COMPONENT_COUNT));
					if(Constants.arrowOn){
						float b1 = perspektive.getRotation();
						float b2 = (float) (new direction(new point(-perspektive.getFieldTranslation()[0],-perspektive.getFieldTranslation()[1]), 
								new point(perspektive.combineLoc[0], perspektive.combineLoc[1]))).getBearing();
						float b22= (float) new direction(new point(-perspektive.getFieldTranslation()[0],-perspektive.getFieldTranslation()[1]), 
								new point(perspektive.combineLoc[0], perspektive.combineLoc[1])).getBearing();
						float b3 = (b2 - b1 + 360)%360;
//						String st = "b1:"+b1+", b2:"+b2+", b22:"+b22+", b3:"+b3+
//									", p1:"+(new point(-perspektive.getFieldTranslation()[0],-perspektive.getFieldTranslation()[1])).toShortJSON()+
//									", p2:"+(new point(perspektive.combineLoc[0], perspektive.combineLoc[1])).toShortJSON();
//						Log.i("ARROW", st);
						perspektive.getProjectionMatrixArrowCircle(projectionMatrixTractor, b2, 
								new float[]{0f,0f});
						textureProgram.setUniforms(projectionMatrixTractor, textureArrow);
						bindData3(textureProgram);
						glDrawArrays(GL_TRIANGLE_FAN, 0, Tractor2d.length/(POSITION_COMPONENT_COUNT+TEXTURE_COORDINATES_COMPONENT_COUNT));
				}
				}
			}else if(Constants.currentMapMode==Constants.mapModeField || Constants.currentMapMode==Constants.mapModeFree){
				float  tempSize = (float) (originalSize*-Math.pow(2, perspektive.getZoom())/Math.max(Math.min((float) -Math.pow(1.7, perspektive.getZoom()+1),(float) -Math.pow(2, 5)),(float) -Math.pow(2, 6)));
				this.setSize(tempSize);
//				float[] trailerLoc = new float[]{location[0]-(float) (Math.sin(rotation)*tempSize*(1-anochorY2D)),location[1]-(float) (Math.cos(rotation)*tempSize*(1-anochorY2D))};
				perspektive.getProjectionMatrixAddtionalTractors(projectionMatrixTractor, -perspektive.getRotation(), new float[]{-perspektive.getFieldTranslation()[0],-perspektive.getFieldTranslation()[1]});
				textureProgram.useProgram();
				textureProgram.setUniforms(projectionMatrixTractor, texture2d);
				bindData(textureProgram,perspektive.getView3d());
				glDrawArrays(GL_TRIANGLE_FAN, 0, Tractor2d.length/(POSITION_COMPONENT_COUNT+TEXTURE_COORDINATES_COMPONENT_COUNT));
				perspektive.getProjectionMatrixAddtionalTractors(projectionMatrixTractor, -perspektive.getRotation(), new float[]{-perspektive.getFieldTranslation()[0],-perspektive.getFieldTranslation()[1]});
				textureProgram.setUniforms(projectionMatrixTractor, textureTrailer2d);
				bindData2(textureProgram);
				glDrawArrays(GL_TRIANGLE_FAN, 0, Tractor2d.length/(POSITION_COMPONENT_COUNT+TEXTURE_COORDINATES_COMPONENT_COUNT));
			}
		}
	}
	public void bindData(TextureShaderProgram textureProgram,boolean graphic3d) {
		if(graphic3d) {
			vertexArray3d.setVertexAttribPointer(0,textureProgram.getPositionAttributeLocation(),POSITION_COMPONENT_COUNT,STRIDE);
			vertexArray3d.setVertexAttribPointer(POSITION_COMPONENT_COUNT,textureProgram.getTextureCoordinatesAttributeLocation(),TEXTURE_COORDINATES_COMPONENT_COUNT,STRIDE);
		}else {
			vertexArray2d.setVertexAttribPointer(0,textureProgram.getPositionAttributeLocation(),POSITION_COMPONENT_COUNT,STRIDE);
			vertexArray2d.setVertexAttribPointer(POSITION_COMPONENT_COUNT,textureProgram.getTextureCoordinatesAttributeLocation(),TEXTURE_COORDINATES_COMPONENT_COUNT,STRIDE);
		}		
	}
	public void bindData2(TextureShaderProgram textureProgram) {
		vertexArray2dTrailer.setVertexAttribPointer(0,textureProgram.getPositionAttributeLocation(),POSITION_COMPONENT_COUNT,STRIDE);
		vertexArray2dTrailer.setVertexAttribPointer(POSITION_COMPONENT_COUNT,textureProgram.getTextureCoordinatesAttributeLocation(),TEXTURE_COORDINATES_COMPONENT_COUNT,STRIDE);
	}
	public void bindData3(TextureShaderProgram textureProgram) {
		vertexArray2dArrow.setVertexAttribPointer(0,textureProgram.getPositionAttributeLocation(),POSITION_COMPONENT_COUNT,STRIDE);
		vertexArray2dArrow.setVertexAttribPointer(POSITION_COMPONENT_COUNT,textureProgram.getTextureCoordinatesAttributeLocation(),TEXTURE_COORDINATES_COMPONENT_COUNT,STRIDE);
	}
}
