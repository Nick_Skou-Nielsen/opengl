package com.opengl.android.Geometric;

import java.util.ArrayList;

import com.opengl.android.Engines.ColorShaderProgram;

import geometricObjects.polygon;

public class ColorMultiPoly extends basicDrawer {
    protected ColorShaderProgram program;
    protected ArrayList<ColorPoly> colorPolys = new ArrayList<ColorPoly>();
    protected ArrayList<ColorLine> colorLines = new ArrayList<ColorLine>();
    
    public ColorMultiPoly(final polygon[] poly,final polygon[] edge,
    		final float[] color,final float thickness,final float[] stroakColor,final int layer){
    	if(edge==null){
    		for(int i=0;i<poly.length;i++){
        		colorPolys.add(new ColorPoly(poly[i], null, color, thickness, stroakColor, layer));
        	}
    	}else{
    		for(int i=0;i<poly.length;i++){
        		colorPolys.add(new ColorPoly(poly[i], null, color, 0, stroakColor, layer));
        	}
    		for(int i=0;i<edge.length;i++){
    			colorLines.add(new ColorLine(edge[i].arcs, stroakColor, thickness, layer));
        	}
    	}
    }
    
    public void replace(final polygon[] poly,final polygon[] edge,
    		final float[] color,final float thickness,final float[] stroakColor,final int layer){
    	colorPolys.clear();
    	colorLines.clear();
    	if(edge==null){
    		for(int i=0;i<poly.length;i++){
        		colorPolys.add(new ColorPoly(poly[i], null, color, thickness, stroakColor, layer));
        	}
    	}else{
    		for(int i=0;i<poly.length;i++){
        		colorPolys.add(new ColorPoly(poly[i], null, color, 0, stroakColor, layer));
        	}
    		for(int i=0;i<edge.length;i++){
    			colorLines.add(new ColorLine(edge[i].arcs, stroakColor, thickness, layer));
        	}
    	}
    	setProgram(program);
    }
    
    public void setProgram(ColorShaderProgram program) {
    	this.program = program;
    	for(int i=0;i<colorPolys.size();i++){
    		colorPolys.get(i).setProgram(program);
    		colorPolys.get(i).visible = true;
    	}
    	for(int i=0;i<colorLines.size();i++){
    		colorLines.get(i).setProgram(program);
    		colorLines.get(i).visible = true;
    	}
    }
    public synchronized void draw() {
    	if(program!=null){
    		for(int i=0;i<colorPolys.size();i++){
        		colorPolys.get(i).draw();
        	}
        	for(int i=0;i<colorLines.size();i++){
        		colorLines.get(i).draw();
        	}
    	}
    }
}
