package com.opengl.android.Geometric;
import static com.opengl.android.Util.Constants.BYTES_PER_FLOAT;

import java.nio.FloatBuffer;

import com.opengl.android.Engines.ColorShaderProgram;
import com.opengl.android.Engines.ShaderProgram;
import com.opengl.android.Engines.TextureShaderProgram;
import static android.opengl.GLES20.*;


import android.content.Context;
import geometricObjects.polygon;

public class ColorPoly extends basicDrawer{
	private static final String A_POSITION = "a_Position";
    private static final String A_COLOR = "a_Color";    
	protected float[] polygon;
	protected VertexArray vertexArray;
    protected ColorShaderProgram program;
    protected static final int POSITION_COMPONENT_COUNT = 2;
    protected static final int COLOR_COMPONENT_COUNT = 4;
    protected static final int STRIDE = (POSITION_COMPONENT_COUNT + COLOR_COMPONENT_COUNT) * BYTES_PER_FLOAT;
    int len;
    protected ColorLine line;
    
    protected ColorPoly() {
    	
    }
    
    public ColorPoly(final polygon poly,final polygon edge,
    		final float[] color,final float thickness,final float[] stroakColor,final int layer) {
    	len = poly.arcs.length;
    	this.layer = layer;
    	if(thickness>0) {
    		if(edge!=null){
    			line = new ColorLine(edge.arcs, stroakColor, thickness, layer);
    		}else{
    			line = new ColorLine(poly.arcs, stroakColor, thickness, layer);
    		}
    	}
    	this.polygon = GeometricTransformation.polygonToVertexArray(poly,color);
    	this.vertexArray = new VertexArray(polygon);
    }
    
    public synchronized void replace(polygon poly, polygon edge, float[] color, float thickness, float[] stroakColor, int layer){
    	len = poly.arcs.length;
    	this.layer = layer;
    	if(thickness>0) {
    		if(edge!=null){
    			if(line!=null){
    				line.replace(edge.arcs, stroakColor, thickness, layer);
    			}else{
    				line = new ColorLine(edge.arcs, stroakColor, thickness, layer);
    			}
    		}else{
    			if(line!=null){
    				line.replace(poly.arcs, stroakColor, thickness, layer);
    			}else{
    				line = new ColorLine(poly.arcs, stroakColor, thickness, layer);
    			}
    		}
    	}
    	this.polygon = GeometricTransformation.polygonToVertexArray(poly,color);
    	this.vertexArray = new VertexArray(polygon);
    }
    
    public void setProgram(ColorShaderProgram program) {
    	this.program = program;
    	if(this.getClass()==ColorPoly.class) {
    		if(line!=null) {
    			line.setProgram(program);
    		}
    	}
    }
    public void bindData() {
    	vertexArray.setVertexAttribPointer(0,program.getPositionAttributeLocation(),POSITION_COMPONENT_COUNT, STRIDE);
    	vertexArray.setVertexAttribPointer(POSITION_COMPONENT_COUNT,program.getColorAttributeLocation(), COLOR_COMPONENT_COUNT,STRIDE);	
    	}
    public synchronized void draw() {
    	if (program!=null) {
    		program.useProgram();
        	program.setUniforms();
        	bindData();
        	if(this.getClass()==ColorPoly.class) {
        		glDrawArrays(GL_TRIANGLES, 0, (len-2)*3);
        		if(line!=null) {
        			line.draw();
        		}
//        		glDrawArrays(GL_LINES, (len-2)*3, polygon.length/(POSITION_COMPONENT_COUNT+COLOR_COMPONENT_COUNT));
        	}else {
        		glDrawArrays(GL_TRIANGLES, 0, polygon.length/(POSITION_COMPONENT_COUNT+COLOR_COMPONENT_COUNT));
        	}
    	}
	}

    
    
}
