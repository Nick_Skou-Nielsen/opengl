package com.opengl.android.Geometric;

import static android.opengl.GLES20.GL_TRIANGLE_FAN;
import static android.opengl.GLES20.glDrawArrays;

import com.opengl.android.R;
import com.opengl.android.Engines.TextureHelper;
import com.opengl.android.Engines.TextureShaderProgram;
import com.opengl.android.Util.PerspectiveHandler;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class Combine3D extends TexturePoly {
	private int tractor2d;
	private int tractor3d;
	private int spout2dOn;
	private int spout3dOn;
	private int spout2dOff;
	private int spout3dOff;
	private int texture2d;
	private int texture3d;
	private int spoutTexture2dOn;
	private int spoutTexture3dOn;
	private int spoutTexture2dOff;
	private int spoutTexture3dOff;
	private boolean spoutOn = false;
	private float anochorX2D;
	private float anochorX3D;
	private float anochorY2D;
	private float anochorY3D;
	private float height2d;
	private float height3d;
	private float width2d;
	private float width3d;
	private final float[] projectionMatrixTractor = new float[16];
	private TextureShaderProgram textureProgram;
	private VertexArray vertexArray2d;
	private VertexArray vertexArray3d;
	private final PerspectiveHandler perspektive;
	private float[] Tractor2d = {
			// Order of coordinates: X, Y, S, T
			// Triangle Fan
			0f, 0f, 0.5f, 0.5f,
			-0.5f, -0.5f, 0f, 1f,
			0.5f, -0.5f, 1f, 1f,
			0.5f, 0.5f, 1f, 0f,
			-0.5f, 0.5f, 0f, 0f,
			-0.5f, -0.5f, 0f, 1f };
	private float[] Tractor3d = {
			// Order of coordinates: X, Y, S, T
			// Triangle Fan
			0f, 0f, 0.5f, 0.5f,
			-0.5f, -0.5f, 0f, 1f,
			0.5f, -0.5f, 1f, 1f,
			0.5f, 0.5f, 1f, 0f,
			-0.5f, 0.5f, 0f, 0f,
			-0.5f, -0.5f, 0f, 1f };
	
	public void useSpout(boolean useSpout){
		this.spoutOn = useSpout;
	}
	
	public void update() {
		texture2d = TextureHelper.loadTexture(textureProgram.getContext(), tractor2d);
		texture3d = TextureHelper.loadTexture(textureProgram.getContext(), tractor3d);
		
		spoutTexture2dOn = TextureHelper.loadTexture(textureProgram.getContext(), spout2dOn);
		spoutTexture3dOn = TextureHelper.loadTexture(textureProgram.getContext(), spout3dOn);
		spoutTexture2dOff = TextureHelper.loadTexture(textureProgram.getContext(), spout2dOff);
		spoutTexture3dOff = TextureHelper.loadTexture(textureProgram.getContext(), spout3dOff);
	}
	
	public void setSize(float size){
		Tractor2d = new float[] {
				// Order of coordinates: X, Y, S, T
				// Triangle Fan
				0f, 0f, 0.5f, 0.5f,
				-0.5f, -0.5f, 0f, 1f,
				0.5f, -0.5f, 1f, 1f,
				0.5f, 0.5f, 1f, 0f,
				-0.5f, 0.5f, 0f, 0f,
				-0.5f, -0.5f, 0f, 1f };
		Tractor3d = new float[]{
				// Order of coordinates: X, Y, S, T
				// Triangle Fan
				0f, 0f, 0.5f, 0.5f,
				-0.5f, -0.5f, 0f, 1f,
				0.5f, -0.5f, 1f, 1f,
				0.5f, 0.5f, 1f, 0f,
				-0.5f, 0.5f, 0f, 0f,
				-0.5f, -0.5f, 0f, 1f };
		if(height2d>width2d) {
			Tractor2d[5] = Tractor2d[5] * height2d/width2d*size+(anochorY2D-0.5f)* height2d/width2d*size;
			Tractor2d[9] = Tractor2d[9] * height2d/width2d*size+(anochorY2D-0.5f)* height2d/width2d*size;
			Tractor2d[13] = Tractor2d[13] * height2d/width2d*size+(anochorY2D-0.5f)* height2d/width2d*size;
			Tractor2d[17] = Tractor2d[17] * height2d/width2d*size+(anochorY2D-0.5f)* height2d/width2d*size;
			Tractor2d[21] = Tractor2d[21] * height2d/width2d*size+(anochorY2D-0.5f)* height2d/width2d*size;
			Tractor2d[4] = Tractor2d[4] * size+(anochorX2D-0.5f)*size;
			Tractor2d[8] = Tractor2d[8] * size+(anochorX2D-0.5f)*size;
			Tractor2d[12] = Tractor2d[12] * size+(anochorX2D-0.5f)*size;
			Tractor2d[16] = Tractor2d[16] * size+(anochorX2D-0.5f)*size;
			Tractor2d[20] = Tractor2d[20] * size+(anochorX2D-0.5f)*size;
			Tractor2d[0] = Tractor2d[0] +(anochorX2D-0.5f)*size;
			Tractor2d[1] = Tractor2d[1] +(anochorY2D-0.5f)* height2d/width2d*size;
		}else {
			Tractor2d[4] = Tractor2d[4] * width2d/height2d*size+(anochorX2D-0.5f)* height2d/width2d*size;
			Tractor2d[8] = Tractor2d[8] * width2d/height2d*size+(anochorX2D-0.5f)* height2d/width2d*size;
			Tractor2d[12] = Tractor2d[12] * width2d/height2d*size+(anochorX2D-0.5f)* height2d/width2d*size;
			Tractor2d[16] = Tractor2d[16] * width2d/height2d*size+(anochorX2D-0.5f)* height2d/width2d*size;
			Tractor2d[20] = Tractor2d[20] * width2d/height2d*size+(anochorX2D-0.5f)* height2d/width2d*size;
			Tractor2d[5] = Tractor2d[5] * size+(anochorY2D-0.5f)*size;
			Tractor2d[9] = Tractor2d[9] * size+(anochorY2D-0.5f)*size;
			Tractor2d[13] = Tractor2d[13] * size+(anochorY2D-0.5f)*size;
			Tractor2d[17] = Tractor2d[17] * size+(anochorY2D-0.5f)*size;
			Tractor2d[21] = Tractor2d[21] * size+(anochorY2D-0.5f)*size;
			Tractor2d[1] = Tractor2d[1] +(anochorY2D-0.5f)*size;
			Tractor2d[0] = Tractor2d[0] +(anochorX2D-0.5f)* height2d/width2d*size;
		}
		vertexArray2d = new VertexArray(Tractor2d);
		if(height3d>width3d) {
			Tractor3d[5] = Tractor3d[5] * height3d/width3d*size+(anochorY3D-0.5f)* height3d/width3d*size;
			Tractor3d[9] = Tractor3d[9] * height3d/width3d*size+(anochorY3D-0.5f)* height3d/width3d*size;
			Tractor3d[13] = Tractor3d[13] * height3d/width3d*size+(anochorY3D-0.5f)* height3d/width3d*size;
			Tractor3d[17] = Tractor3d[17] * height3d/width3d*size+(anochorY3D-0.5f)* height3d/width3d*size;
			Tractor3d[21] = Tractor3d[21] * height3d/width3d*size+(anochorY3D-0.5f)* height3d/width3d*size;
			Tractor3d[4] = Tractor3d[4] * size+(anochorX3D-0.5f)*size;
			Tractor3d[8] = Tractor3d[8] * size+(anochorX3D-0.5f)*size;
			Tractor3d[12] = Tractor3d[12] * size+(anochorX3D-0.5f)*size;
			Tractor3d[16] = Tractor3d[16] * size+(anochorX3D-0.5f)*size;
			Tractor3d[20] = Tractor3d[20] * size+(anochorX3D-0.5f)*size;
			Tractor3d[0] = Tractor3d[0] +(anochorX3D-0.5f);
			Tractor3d[1] = Tractor3d[1] +(anochorY3D-0.5f)* height3d/width3d*size;
		}else {
			Tractor3d[4] = Tractor3d[4] * width3d/height3d*size+(anochorX3D-0.5f)* height3d/width3d*size;
			Tractor3d[8] = Tractor3d[8] * width3d/height3d*size+(anochorX3D-0.5f)* height3d/width3d*size;
			Tractor3d[12] = Tractor3d[12] * width3d/height3d*size+(anochorX3D-0.5f)* height3d/width3d*size;
			Tractor3d[16] = Tractor3d[16] * width3d/height3d*size+(anochorX3D-0.5f)* height3d/width3d*size;
			Tractor3d[20] = Tractor3d[20] * width3d/height3d*size+(anochorX3D-0.5f)* height3d/width3d*size;
			Tractor3d[5] = Tractor3d[5] * size+(anochorY3D-0.5f)*size;
			Tractor3d[9] = Tractor3d[9] * size+(anochorY3D-0.5f)*size;
			Tractor3d[13] = Tractor3d[13] * size+(anochorY3D-0.5f)*size;
			Tractor3d[17] = Tractor3d[17] * size+(anochorY3D-0.5f)*size;
			Tractor3d[21] = Tractor3d[21] * size+(anochorY3D-0.5f)*size;
			Tractor3d[0] = Tractor3d[0] +(anochorX3D-0.5f)* height3d/width3d*size;
			Tractor3d[1] = Tractor3d[1] +(anochorY3D-0.5f)*size;
		}
		vertexArray3d = new VertexArray(Tractor3d);
	}

	public Combine3D(final Context context,final float size,final int tractor2d,
			final int tractor3d,final PerspectiveHandler perspektive,final int layer,final float anochorX2D,
			final float anochorY2D,final float anochorX3D,final float anochorY3D
			,final int spout2dOn,final int spout3dOn,final int spout2dOff,final int spout3dOff,
			final boolean spoutOn) {
		super();
		this.anochorY2D = anochorY2D;
		this.anochorX2D = anochorX2D;
		this.anochorY3D = anochorY3D;
		this.anochorX3D = anochorX3D;
		this.layer = layer;
		this.spoutOn = spoutOn;
		this.tractor2d = tractor2d;
		this.tractor3d = tractor3d;
		this.spout2dOn = spout2dOn;
		this.spout3dOn = spout3dOn;
		this.spout2dOff = spout2dOff;
		this.spout3dOff = spout3dOff;
		BitmapFactory.Options dimensions = new BitmapFactory.Options(); 
		dimensions.inJustDecodeBounds = true;
		Bitmap mBitmap = BitmapFactory.decodeResource(context.getResources(), tractor2d, dimensions);
		height2d = dimensions.outHeight;
		width2d =  dimensions.outWidth;
		if(height2d>width2d) {
			Tractor2d[5] = Tractor2d[5] * height2d/width2d*size+(anochorY2D-0.5f)* height2d/width2d*size;
			Tractor2d[9] = Tractor2d[9] * height2d/width2d*size+(anochorY2D-0.5f)* height2d/width2d*size;
			Tractor2d[13] = Tractor2d[13] * height2d/width2d*size+(anochorY2D-0.5f)* height2d/width2d*size;
			Tractor2d[17] = Tractor2d[17] * height2d/width2d*size+(anochorY2D-0.5f)* height2d/width2d*size;
			Tractor2d[21] = Tractor2d[21] * height2d/width2d*size+(anochorY2D-0.5f)* height2d/width2d*size;
			Tractor2d[4] = Tractor2d[4] * size+(anochorX2D-0.5f)*size;
			Tractor2d[8] = Tractor2d[8] * size+(anochorX2D-0.5f)*size;
			Tractor2d[12] = Tractor2d[12] * size+(anochorX2D-0.5f)*size;
			Tractor2d[16] = Tractor2d[16] * size+(anochorX2D-0.5f)*size;
			Tractor2d[20] = Tractor2d[20] * size+(anochorX2D-0.5f)*size;
			Tractor2d[0] = Tractor2d[0] +(anochorX2D-0.5f)*size;
			Tractor2d[1] = Tractor2d[1] +(anochorY2D-0.5f)* height2d/width2d*size;
		}else {
			Tractor2d[4] = Tractor2d[4] * width2d/height2d*size+(anochorX2D-0.5f)* height2d/width2d*size;
			Tractor2d[8] = Tractor2d[8] * width2d/height2d*size+(anochorX2D-0.5f)* height2d/width2d*size;
			Tractor2d[12] = Tractor2d[12] * width2d/height2d*size+(anochorX2D-0.5f)* height2d/width2d*size;
			Tractor2d[16] = Tractor2d[16] * width2d/height2d*size+(anochorX2D-0.5f)* height2d/width2d*size;
			Tractor2d[20] = Tractor2d[20] * width2d/height2d*size+(anochorX2D-0.5f)* height2d/width2d*size;
			Tractor2d[5] = Tractor2d[5] * size+(anochorY2D-0.5f)*size;
			Tractor2d[9] = Tractor2d[9] * size+(anochorY2D-0.5f)*size;
			Tractor2d[13] = Tractor2d[13] * size+(anochorY2D-0.5f)*size;
			Tractor2d[17] = Tractor2d[17] * size+(anochorY2D-0.5f)*size;
			Tractor2d[21] = Tractor2d[21] * size+(anochorY2D-0.5f)*size;
			Tractor2d[1] = Tractor2d[1] +(anochorY2D-0.5f)*size;
			Tractor2d[0] = Tractor2d[0] +(anochorX2D-0.5f)* height2d/width2d*size;
		}
		vertexArray2d = new VertexArray(Tractor2d);
		mBitmap = BitmapFactory.decodeResource(context.getResources(), tractor3d, dimensions);
		height3d = dimensions.outHeight;
		width3d =  dimensions.outWidth;
		if(height3d>width3d) {
			Tractor3d[5] = Tractor3d[5] * height3d/width3d*size+(anochorY3D-0.5f)* height3d/width3d*size;
			Tractor3d[9] = Tractor3d[9] * height3d/width3d*size+(anochorY3D-0.5f)* height3d/width3d*size;
			Tractor3d[13] = Tractor3d[13] * height3d/width3d*size+(anochorY3D-0.5f)* height3d/width3d*size;
			Tractor3d[17] = Tractor3d[17] * height3d/width3d*size+(anochorY3D-0.5f)* height3d/width3d*size;
			Tractor3d[21] = Tractor3d[21] * height3d/width3d*size+(anochorY3D-0.5f)* height3d/width3d*size;
			Tractor3d[4] = Tractor3d[4] * size+(anochorX3D-0.5f)*size;
			Tractor3d[8] = Tractor3d[8] * size+(anochorX3D-0.5f)*size;
			Tractor3d[12] = Tractor3d[12] * size+(anochorX3D-0.5f)*size;
			Tractor3d[16] = Tractor3d[16] * size+(anochorX3D-0.5f)*size;
			Tractor3d[20] = Tractor3d[20] * size+(anochorX3D-0.5f)*size;
			Tractor3d[0] = Tractor3d[0] +(anochorX3D-0.5f);
			Tractor3d[1] = Tractor3d[1] +(anochorY3D-0.5f)* height3d/width3d*size;
		}else {
			Tractor3d[4] = Tractor3d[4] * width3d/height3d*size+(anochorX3D-0.5f)* height3d/width3d*size;
			Tractor3d[8] = Tractor3d[8] * width3d/height3d*size+(anochorX3D-0.5f)* height3d/width3d*size;
			Tractor3d[12] = Tractor3d[12] * width3d/height3d*size+(anochorX3D-0.5f)* height3d/width3d*size;
			Tractor3d[16] = Tractor3d[16] * width3d/height3d*size+(anochorX3D-0.5f)* height3d/width3d*size;
			Tractor3d[20] = Tractor3d[20] * width3d/height3d*size+(anochorX3D-0.5f)* height3d/width3d*size;
			Tractor3d[5] = Tractor3d[5] * size+(anochorY3D-0.5f)*size;
			Tractor3d[9] = Tractor3d[9] * size+(anochorY3D-0.5f)*size;
			Tractor3d[13] = Tractor3d[13] * size+(anochorY3D-0.5f)*size;
			Tractor3d[17] = Tractor3d[17] * size+(anochorY3D-0.5f)*size;
			Tractor3d[21] = Tractor3d[21] * size+(anochorY3D-0.5f)*size;
			Tractor3d[0] = Tractor3d[0] +(anochorX3D-0.5f)* height3d/width3d*size;
			Tractor3d[1] = Tractor3d[1] +(anochorY3D-0.5f)*size;
		}
		vertexArray3d = new VertexArray(Tractor3d);
		this.perspektive = perspektive;
	}
	public void setProgram(TextureShaderProgram program) {
		this.textureProgram = program;
	}
	public synchronized void draw() {
		if(textureProgram!=null) {
			textureProgram.useProgram();
			perspektive.getProjectionMatrixTractor(projectionMatrixTractor);
			if(perspektive.getView3d()) {
				textureProgram.setUniforms(projectionMatrixTractor, texture3d);
			}else {
				textureProgram.setUniforms(projectionMatrixTractor, texture2d);
			}
			bindData(textureProgram,perspektive.getView3d());
			glDrawArrays(GL_TRIANGLE_FAN, 0, Tractor2d.length/(POSITION_COMPONENT_COUNT+TEXTURE_COORDINATES_COMPONENT_COUNT));
			if(perspektive.getView3d()) {
				if(spoutOn){
					textureProgram.setUniforms(projectionMatrixTractor, spoutTexture3dOn);
				}else{
					textureProgram.setUniforms(projectionMatrixTractor, spoutTexture3dOff);
				}
			}else {
				if(spoutOn){
					textureProgram.setUniforms(projectionMatrixTractor, spoutTexture2dOn);
				}else{
					textureProgram.setUniforms(projectionMatrixTractor, spoutTexture2dOff);
				}
			}
			bindData(textureProgram,perspektive.getView3d());
			glDrawArrays(GL_TRIANGLE_FAN, 0, Tractor2d.length/(POSITION_COMPONENT_COUNT+TEXTURE_COORDINATES_COMPONENT_COUNT));
		}
	}
	public void bindData(TextureShaderProgram textureProgram,boolean graphic3d) {
		if(graphic3d) {
			vertexArray3d.setVertexAttribPointer(0,textureProgram.getPositionAttributeLocation(),POSITION_COMPONENT_COUNT,STRIDE);
			vertexArray3d.setVertexAttribPointer(POSITION_COMPONENT_COUNT,textureProgram.getTextureCoordinatesAttributeLocation(),TEXTURE_COORDINATES_COMPONENT_COUNT,STRIDE);
		}else {
			vertexArray2d.setVertexAttribPointer(0,textureProgram.getPositionAttributeLocation(),POSITION_COMPONENT_COUNT,STRIDE);
			vertexArray2d.setVertexAttribPointer(POSITION_COMPONENT_COUNT,textureProgram.getTextureCoordinatesAttributeLocation(),TEXTURE_COORDINATES_COMPONENT_COUNT,STRIDE);
		}		
	}
}
