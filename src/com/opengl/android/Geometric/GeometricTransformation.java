package com.opengl.android.Geometric;

import java.util.ArrayList;

import android.util.Log;
import geometricObjects.arc;
import geometricObjects.point;
import geometricObjects.polygon;

public class GeometricTransformation {
	
	public static float[] makeTextureSquare(float size, float anchorX, float anchorY, float height, float width){
		float[] Out = {
				// Order of coordinates: X, Y, S, T
				0f, 0f, 0.5f, 0.5f,
				-0.5f, -0.5f, 0f, 1f,
				0.5f, -0.5f, 1f, 1f,
				0.5f, 0.5f, 1f, 0f,
				-0.5f, 0.5f, 0f, 0f,
				-0.5f, -0.5f, 0f, 1f };
		
		if(height>width) {
			Out[5] = Out[5] * height/width*size+(anchorY-0.5f)* height/width*size;
			Out[9] = Out[9] * height/width*size+(anchorY-0.5f)* height/width*size;
			Out[13] = Out[13] * height/width*size+(anchorY-0.5f)* height/width*size;
			Out[17] = Out[17] * height/width*size+(anchorY-0.5f)* height/width*size;
			Out[21] = Out[21] * height/width*size+(anchorY-0.5f)* height/width*size;
			Out[4] = Out[4] * size+(anchorX-0.5f)*size;
			Out[8] = Out[8] * size+(anchorX-0.5f)*size;
			Out[12] = Out[12] * size+(anchorX-0.5f)*size;
			Out[16] = Out[16] * size+(anchorX-0.5f)*size;
			Out[20] = Out[20] * size+(anchorX-0.5f)*size;
			Out[0] = Out[0] +(anchorX-0.5f)*size;
			Out[1] = Out[1] +(anchorY-0.5f)* height/width*size;
		}else {
			Out[4] = Out[4] * width/height*size+(anchorX-0.5f)* height/width*size;
			Out[8] = Out[8] * width/height*size+(anchorX-0.5f)* height/width*size;
			Out[12] = Out[12] * width/height*size+(anchorX-0.5f)* height/width*size;
			Out[16] = Out[16] * width/height*size+(anchorX-0.5f)* height/width*size;
			Out[20] = Out[20] * width/height*size+(anchorX-0.5f)* height/width*size;
			Out[5] = Out[5] * size+(anchorY-0.5f)*size;
			Out[9] = Out[9] * size+(anchorY-0.5f)*size;
			Out[13] = Out[13] * size+(anchorY-0.5f)*size;
			Out[17] = Out[17] * size+(anchorY-0.5f)*size;
			Out[21] = Out[21] * size+(anchorY-0.5f)*size;
			Out[1] = Out[1] +(anchorY-0.5f)*size;
			Out[0] = Out[0] +(anchorX-0.5f)* height/width*size;
		}
		
		return Out;
		
	}
	
	public static float[] polygonToVertexArray(polygon poly, float[] color) {
		polygon[] polygons = polygonToTriangles(poly);
		float[] tempList = new float[polygons.length*6*3+poly.arcs.length*6*2];
		for(int i=0; i<polygons.length;i++) {
			for(int j=0;j<3;j++) {
				tempList[i*18+j*6] = (float) polygons[i].arcs[j].startPoint.x;
				tempList[i*18+j*6+1] = (float) polygons[i].arcs[j].startPoint.y;
				tempList[i*18+j*6+2] = color[0];
				tempList[i*18+j*6+3] = color[1];
				tempList[i*18+j*6+4] = color[2];
				tempList[i*18+j*6+5] = color[3];
			}
		}
		for(int i=0;i<poly.arcs.length;i++) {
			tempList[polygons.length*6*3+i*12] = (float) poly.arcs[i].startPoint.x;
			tempList[polygons.length*6*3+i*12+1] = (float) poly.arcs[i].startPoint.y;
			tempList[polygons.length*6*3+i*12+2] = 0f;
			tempList[polygons.length*6*3+i*12+3] = 0f;
			tempList[polygons.length*6*3+i*12+4] = 0f;
			tempList[polygons.length*6*3+i*12+5] = 1f;
			tempList[polygons.length*6*3+i*12+6] = (float) poly.arcs[i].pointOnArc(1).x;
			tempList[polygons.length*6*3+i*12+7] = (float) poly.arcs[i].pointOnArc(1).y;
			tempList[polygons.length*6*3+i*12+8] = 0f;
			tempList[polygons.length*6*3+i*12+9] = 0f;
			tempList[polygons.length*6*3+i*12+10] = 0f;
			tempList[polygons.length*6*3+i*12+11] = 1f;
		}
		return tempList;
	}
	public static float[] arcToVertexArray(arc[] line, float[] color, double thickness) {
		ArrayList<polygon> polygonsList = new ArrayList<polygon>();
		for(int i=0;i<line.length;i++) {
			arc temp1 = new arc(line[i].startPoint,line[i].startDirection,thickness/2);
			arc temp2 = new arc(line[i].pointOnArc(1),line[i].directionOnArc(1),thickness/2);
			point[] points = new point[4];
			points[0] = temp1.pivot(temp1.startPoint, 90).pointOnArc(1);
			points[1] = temp2.pivot(temp2.startPoint, 90).pointOnArc(1);
			points[2] = temp2.pivot(temp2.startPoint, -90).pointOnArc(1);
			points[3] = temp1.pivot(temp1.startPoint, -90).pointOnArc(1);
//			polygon[] tempPoly = polygonToTriangles(new polygon(points));
			polygonsList.add(new polygon(new point[] {points[0], points[1],  points[2]}));
			polygonsList.add(new polygon(new point[] {points[3], points[0],  points[2]}));
		}
		polygon[] polygons = new polygon[polygonsList.size()];
		polygonsList.toArray(polygons);
		float[] tempList = new float[polygons.length*6*3];
		for(int i=0; i<polygons.length;i++) {
			for(int j=0;j<3;j++) {
				tempList[i*18+j*6] = (float) polygons[i].arcs[j].startPoint.x;
				tempList[i*18+j*6+1] = (float) polygons[i].arcs[j].startPoint.y;
				tempList[i*18+j*6+2] = color[0];
				tempList[i*18+j*6+3] = color[1];
				tempList[i*18+j*6+4] = color[2];
				tempList[i*18+j*6+5] = color[3];
			}
		}
		return tempList;
	}
	public static float[] pointToVertexArray(point point, float[] color, double size) {
		float[] tempList = new float[6*14];
		tempList[0] = (float) point.x;
		tempList[1] = (float) point.y;
		tempList[2] = color[0];
		tempList[3] = color[1];
		tempList[4] = color[2];
		tempList[5] = color[3];
		for(int i=0; i<13;i++) {
			tempList[i*6+6] = (float) (point.x+size/2*Math.cos(2*Math.PI*i/12));
			tempList[i*6+1+6] = (float) (point.y+size/2*Math.sin(2*Math.PI*i/12));
			tempList[i*6+2+6] = color[0];
			tempList[i*6+3+6] = color[1];
			tempList[i*6+4+6] = color[2];
			tempList[i*6+5+6] = color[3];
		}
		return tempList;
	}
	
	public static polygon[] polygonToTriangles(polygon poly) {
		//Improve this function for performance
		polygon[] outPoly = new polygon[poly.arcs.length-2];
		boolean finished = true;
		int offset = 0;
		while(poly.arcs.length>3) {
			boolean foundTriangle = false;
			for(int i=0+offset; i<poly.arcs.length+offset;i++) {
				arc[] tempArcs = new arc[3];
				tempArcs[0] = poly.arcs[(((i-1)%poly.arcs.length)+poly.arcs.length)%poly.arcs.length];
				tempArcs[1] = poly.arcs[(((i)%poly.arcs.length)+poly.arcs.length)%poly.arcs.length];
				tempArcs[2] = new arc(tempArcs[1].pointOnArc(1),tempArcs[0].startPoint);
				polygon tempPoly = new polygon(tempArcs);
				point[] tempPoints = new point[poly.arcs.length-1];
				int ind = 0;
				for(int j=0; j<poly.arcs.length;j++) {
					if(j!=i) {
						tempPoints[j-ind] = poly.arcs[j].startPoint;
					}else {
						ind = 1;
					}
				}
				polygon tempPoly2 = new polygon(tempPoints);
//				if((poly.isPointInPolygon(tempArcs[2].pointOnArc(0.5)) || poly.isPointOnPolygon(tempArcs[2].pointOnArc(0.5)))){
				if(Math.abs(tempPoly.area()+tempPoly2.area()-poly.area())<1e-4 && ((poly.isPointInPolygon(tempArcs[2].pointOnArc(0.5)) || poly.isPointOnPolygon(tempArcs[2].pointOnArc(0.5))))){
					polygon[] tempPolys = tempPoly.overlapPolygon(poly);
					if(tempPolys.length==1 & tempPoly.area()==tempPolys[0].area()) {
//						point[] tempPoints = new point[poly.arcs.length-1];
//						int ind = 0;
//						for(int j=0; j<poly.arcs.length;j++) {
//							if(j!=i) {
//								tempPoints[j-ind] = poly.arcs[j].startPoint;
//							}else {
//								ind = 1;
//							}
//						}
					outPoly[outPoly.length-(poly.arcs.length-2)] = new polygon(tempArcs);
					poly = tempPoly2;
					foundTriangle = true;
					break;
//					if(!tempPoly2.isSelfIntersecting()){
//							poly = tempPoly2;
//							foundTriangle = true;
//							break;
//						}
					}
				}
			}
			if(!foundTriangle){
				finished = false;
				break;
			}
		}
		if(finished){
			outPoly[outPoly.length-1] = poly;
			return outPoly;
		}else{
			if(poly.isSelfIntersecting()){
				polygon[] tempPoly = poly.splitSelfIntersecting();
				int c = 0;
				while(outPoly[c]!=null){
					c++;
				}
				polygon[] outPoly2 = new polygon[c];
				for(int i=0;i<outPoly2.length;i++){
					outPoly2[i] = outPoly[i];
				}
				outPoly = outPoly2;
				for(int i=0;i<tempPoly.length;i++){
					polygon[] splitedPolys = polygonToTriangles(tempPoly[i]);
					outPoly2 = new polygon[splitedPolys.length+outPoly.length];
					for(int j=0;j<outPoly.length;j++){
						outPoly2[j] = outPoly[j];
					}
					for(int j=0;j<splitedPolys.length;j++){
						outPoly2[j+outPoly.length] = splitedPolys[j];
					}
					outPoly = outPoly2;
				}
				return outPoly;
			}
			Log.i("AIMap", "Could not seperate into triangles " + poly.toJSON());
			return null;
		}
		
	}
	public static polygon[] spiltPolygonIteration(polygon poly) {
		polygon[] outPoly = new polygon[2];
		for(int i=0; i<poly.arcs.length;i++) {
			arc[] tempArcs = new arc[3];
			tempArcs[0] = poly.arcs[(((i-1)%poly.arcs.length)+poly.arcs.length)%poly.arcs.length];
			tempArcs[1] = poly.arcs[(((i)%poly.arcs.length)+poly.arcs.length)%poly.arcs.length];
			tempArcs[2] = new arc(tempArcs[1].pointOnArc(1),tempArcs[0].startPoint);
			polygon tempPoly = new polygon(tempArcs);
			if((poly.isPointInPolygon(tempArcs[2].pointOnArc(0.5)) || poly.isPointOnPolygon(tempArcs[2].pointOnArc(0.5)))){
				polygon[] tempPolys = tempPoly.overlapPolygon(poly);
//				if(tempPolys.length==1 & Math.abs(tempPoly.area()-tempPolys[0].area())<1e-6){
				if(Math.abs(tempPoly.area()-tempPolys[0].area())<1e-4){
				outPoly[0] = new polygon(tempArcs);
					point[] tempPoints = new point[poly.arcs.length-1];
					int ind = 0;
					for(int j=0; j<poly.arcs.length;j++) {
						if(j!=i) {
							tempPoints[j-ind] = poly.arcs[j].startPoint;
						}else {
							ind = 1;
						}
					}
					poly = new polygon(tempPoints);
					if(!poly.isSelfIntersecting()){
						break;
					}
				}
			}
		}
		if(outPoly[0]==null){
			return null;
		}else{
			outPoly[1]=poly;
			return outPoly;
		}
	}
}