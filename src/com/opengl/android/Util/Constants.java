package com.opengl.android.Util;

public class Constants {
	public static final int BYTES_PER_FLOAT = 4;
	public static boolean backGroundMap = false;
	public static boolean satMap = false;
	public static boolean basisMap = false;
	public static boolean arrowOn = true;
	
	public final static int mapModeVehicle = 0;
	public final static int mapModeField = 1;
	public final static int mapModeFree = 2;
	
	public static int currentMapMode = 0;
}
