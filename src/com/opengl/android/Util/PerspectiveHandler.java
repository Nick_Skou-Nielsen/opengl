package com.opengl.android.Util;

import static android.opengl.Matrix.multiplyMM;
import static android.opengl.Matrix.rotateM;
import static android.opengl.Matrix.setIdentityM;
import static android.opengl.Matrix.translateM;

import com.opengl.android.Geometric.MatrixHelper;
import com.opengl.android.Geometric.basicDrawer;

public class PerspectiveHandler {
	private final float[] modelMatrix = new float[16]; //Helper for the projection matrix
	private boolean view3d = false; //Sets if we see the program in 3d
	private float zoom = 4; //Sets the zoom level the higher value the father away
	private float angle3d = 58; //Sets the angle of the 3dview
	private float rotation = 180; //Handles rotation of the field
	private float width;
	private float height;
	private final float[] fieldTranslation = new float[] {0f,0f}; //Handles the translation of the field that is to say the viewpoint
	private final float[] tractorTranslation = new float[] {0f,0f}; //Handles the translation of the tractor in relation to the viewpoint
	public final float[] combineLoc = new float[] {0f,0f};
	public final float[] position = new float[] {0f,0f};
	public final float[] fieldCenter = new float[] {0f,0f};
	public final float[] fieldFree = new float[] {0f,0f};
	
	public PerspectiveHandler() {
		
	}
	public void setFieldCenter(float[] center){
		fieldCenter[0] = center[0];
		fieldCenter[1] = center[1];
	}
	public void setFieldFree(float[] Pos){
		fieldFree[0] = Pos[0]+fieldFree[0];
		fieldFree[1] = Pos[1]+fieldFree[1];
		if(Constants.currentMapMode!=Constants.mapModeFree){
			fieldFree[0] = 0;
			fieldFree[1] = 0;
		}
	}
	public float[] getFieldFree(){
		return new float[]{this.fieldFree[0],this.fieldFree[1]};
	}
	
	public float[] getposition(){
		return new float[]{this.position[0],this.position[1]};
	}
	
	public float[] getTractorTranslation(){
		return new float[]{this.tractorTranslation[0],this.tractorTranslation[1]};
	}
	
	public float[] getFieldTranslation(){
		return new float[]{this.fieldTranslation[0],this.fieldTranslation[1]};
	}
	
	
	public void setLocation(basicDrawer vehicle,final float[] location){
		combineLoc[0] = location[0];
		combineLoc[1] = location[1];
	}
	
	
	/**
	 * Sets the information about the devices screen dimensions,
	 * @param width The width of the screen
	 * @param height The Height of the screen
	 */
	public void setDim(final float width,final float height) {
		this.width=width;
		this.height=height;
	}
	/**
	 * @return The current zoom level
	 */
	public float getZoom(){
		return zoom;
	}
	/**
	 * @param zoom sets the zoom level divided by half 
	 * (This is hack to make more possible levels of the zoom)
	 */
	public void setZoom(final float zoom) {
		this.zoom=zoom/2;
	}
	/**
	 * @return Returns whether the map is 3d or not.
	 */
	public boolean getView3d() {
		return view3d;
	}
	/**
	 * @param view3d Sets if the map should be in 3d.
	 */
	public void setView3d(boolean view3d) {
		this.view3d = view3d;
	}
	/**
	 * @return Returns the map rotation in radians
	 */
	public float getRotation() {
		return rotation;
	}
	/**
	 * @param rotation Sets the map rotation in radians
	 */
	public void setRotation(final float rotation) {
		this.rotation = rotation;
	}
	public float getAngle3d() {
		return angle3d;
	}
	public void setAngle3d(final float angle3d) {
		this.angle3d=angle3d;
	}
	public void setFieldTranslation(final float[] fieldTranslation) {
		if(fieldTranslation.length==2) {
			this.fieldTranslation[0] = -fieldTranslation[0];
			this.fieldTranslation[1] = -fieldTranslation[1];
		}
	}
	public void setTractorTranslation(final float[] fieldTranslation) {
		if(fieldTranslation.length==2) {
			this.tractorTranslation[0] = -fieldTranslation[0];
			this.tractorTranslation[1] = -fieldTranslation[1];
		}
	}
	public void getProjectionMatrixField(final float[] projectionMatrixFieldIn) {
		MatrixHelper.perspectiveM(projectionMatrixFieldIn, 45, width/height, 0f, 10f);
		setIdentityM(modelMatrix, 0);
		translateM(modelMatrix, 0, 0f, 0f, (float) -Math.pow(2, zoom));
		if(view3d) {
        	rotateM(modelMatrix, 0, -angle3d, 1f, 0f, 0f); // 3d 
        }
		if(Constants.currentMapMode==Constants.mapModeVehicle){
			rotateM(modelMatrix, 0, rotation, 0f, 0f, 1f);
			translateM(modelMatrix, 0, fieldTranslation[0], fieldTranslation[1], 0f);
		}else if(Constants.currentMapMode==Constants.mapModeField){
			rotateM(modelMatrix, 0, 0f, 0f, 0f, 1f);
			translateM(modelMatrix, 0, fieldCenter[0], fieldCenter[1], 0f);
		}else if(Constants.currentMapMode==Constants.mapModeFree){
			rotateM(modelMatrix, 0, 0f, 0f, 0f, 1f);
			translateM(modelMatrix, 0, fieldFree[0], fieldFree[1], 0f);
		}
		final float[] temp = new float[16];
        multiplyMM(temp, 0, projectionMatrixFieldIn, 0, modelMatrix, 0);
        System.arraycopy(temp, 0, projectionMatrixFieldIn, 0, temp.length);		
	}
	public void getProjectionMatrixTractor(final float[] projectionMatrixTractorIn) {
		MatrixHelper.perspectiveM(projectionMatrixTractorIn, 45, width/height, 0f, 10f);
		setIdentityM(modelMatrix, 0);
		if(view3d) {
			translateM(modelMatrix, 0, tractorTranslation[0], tractorTranslation[1], Math.max(Math.min((float) -Math.pow(1.7, zoom+1),(float) -Math.pow(2, 5)),(float) -Math.pow(2, 6)));
		}else {
			translateM(modelMatrix, 0, tractorTranslation[0], tractorTranslation[1], Math.max(Math.min((float) -Math.pow(1.7, zoom+1),(float) -Math.pow(2, 5.5)),(float) -Math.pow(2, 6)));
		}
		
		final float[] temp = new float[16];
        multiplyMM(temp, 0, projectionMatrixTractorIn, 0, modelMatrix, 0);
        System.arraycopy(temp, 0, projectionMatrixTractorIn, 0, temp.length);
	}
	public void getProjectionMap(final float[] projectionMatrixTractorIn, float[] position) {
		MatrixHelper.perspectiveM(projectionMatrixTractorIn, 45, width/height, 0f, 10f);
		setIdentityM(modelMatrix, 0);
		translateM(modelMatrix, 0, 0f, 0f, (float) -Math.pow(2, zoom));
		if(view3d) {
        	rotateM(modelMatrix, 0, -angle3d, 1f, 0f, 0f); // 3d 
        }
		rotateM(modelMatrix, 0, rotation, 0f, 0f, 1f);
		translateM(modelMatrix, 0, position[0]+fieldTranslation[0], position[1]+fieldTranslation[1], 0f);
		this.position[0] = position[0]+fieldTranslation[0];
		this.position[1] = position[1]+fieldTranslation[1];
		final float[] temp = new float[16];
        multiplyMM(temp, 0, projectionMatrixTractorIn, 0, modelMatrix, 0);
        System.arraycopy(temp, 0, projectionMatrixTractorIn, 0, temp.length);
	}
	public void getProjectionMatrixAddtionalTractors(final float[] projectionMatrixAddtionalTractorIn, float tractorRotation, float[] tractorLocation) {
		MatrixHelper.perspectiveM(projectionMatrixAddtionalTractorIn, 45, width/height, 0f, 10f);
		setIdentityM(modelMatrix, 0);
		translateM(modelMatrix, 0, 0f, 0f, (float) -Math.pow(2, zoom));
		if(view3d) {
        	rotateM(modelMatrix, 0, -angle3d, 1f, 0f, 0f); // 3d 
        }if(Constants.currentMapMode==Constants.mapModeVehicle){
			rotateM(modelMatrix, 0, rotation, 0f, 0f, 1f);
			translateM(modelMatrix, 0, fieldTranslation[0]+tractorLocation[0], fieldTranslation[1]+tractorLocation[1], 0f);
		}else if(Constants.currentMapMode==Constants.mapModeField){
			rotateM(modelMatrix, 0, 0f, 0f, 0f, 1f);
			translateM(modelMatrix, 0, fieldCenter[0]+tractorLocation[0], fieldCenter[1]+tractorLocation[1], 0f);
		}else if(Constants.currentMapMode==Constants.mapModeFree){
			rotateM(modelMatrix, 0, 0f, 0f, 0f, 1f);
			translateM(modelMatrix, 0, fieldFree[0]+tractorLocation[0], fieldFree[1]+tractorLocation[1], 0f);
		}
		final float[] temp = new float[16];
        multiplyMM(temp, 0, projectionMatrixAddtionalTractorIn, 0, modelMatrix, 0);
        System.arraycopy(temp, 0, projectionMatrixAddtionalTractorIn, 0, temp.length);
        rotateM(projectionMatrixAddtionalTractorIn, 0, tractorRotation, 0f, 0f, 1f);        
	}
	
	public void getProjectionMatrixArrowCircle(final float[] projectionMatrixAddtionalTractorIn, float tractorRotation, float[] tractorLocation) {
		MatrixHelper.perspectiveM(projectionMatrixAddtionalTractorIn, 45, width/height, 0f, 10f);
		setIdentityM(modelMatrix, 0);
		if(view3d) {
			translateM(modelMatrix, 0, tractorTranslation[0], tractorTranslation[1], Math.max(Math.min((float) -Math.pow(1.7, zoom+1),(float) -Math.pow(2, 5)),(float) -Math.pow(2, 6)));
		}else {
			translateM(modelMatrix, 0, tractorTranslation[0], tractorTranslation[1], Math.max(Math.min((float) -Math.pow(1.7, zoom+1),(float) -Math.pow(2, 5.5)),(float) -Math.pow(2, 6)));
		}
		rotateM(modelMatrix, 0, -tractorRotation+rotation, 0f, 0f, 1f);
		final float[] temp = new float[16];
        multiplyMM(temp, 0, projectionMatrixAddtionalTractorIn, 0, modelMatrix, 0);
        System.arraycopy(temp, 0, projectionMatrixAddtionalTractorIn, 0, temp.length);        
	}

}
